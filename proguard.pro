###################
#				  #
# 	 Author :     #
#    Alwyn974     #
#				  #
###################

-libraryjars <java.home>/lib/rt.jar
 
-dontoptimize
-dontpreverify
-dontwarn ** 
-keepattributes *Annotation*

-printmapping "./build/libs/novacraft.map"
 
-keep public class re.alwyn974.novacraft.** {
   public protected <methods>;
}
 
-keepclassmembers !public class re.alwyn974.novacraft.** {
   public protected <methods>;
}
 
-keep public class net.minecraft.** {
*;
}
 
-keepclassmembers enum  * {
   public static **[] values();
   public static ** valueOf(java.lang.String);
}