/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.ModMetadata;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraftforge.common.util.EnumHelper;
import re.alwyn974.novacraft.block.entity.TileEntityGraphiteChest;
import re.alwyn974.novacraft.client.events.EventsClient;
import re.alwyn974.novacraft.client.gui.GuiHandler;
import re.alwyn974.novacraft.common.entity.EntityPetard;
import re.alwyn974.novacraft.common.register.ArmorRegister;
import re.alwyn974.novacraft.common.register.BlockRegister;
import re.alwyn974.novacraft.common.register.CraftRegister;
import re.alwyn974.novacraft.common.register.ItemRegister;
import re.alwyn974.novacraft.common.register.PotionRegister;
import re.alwyn974.novacraft.common.register.SeedGenRegister;
import re.alwyn974.novacraft.common.register.ToolRegister;
import re.alwyn974.novacraft.common.register.WeaponRegister;
import re.alwyn974.novacraft.common.register.WorldRegister;
import re.alwyn974.novacraft.events.EventsCommon;
import re.alwyn974.novacraft.events.PotionEvent;
import re.alwyn974.novacraft.network.packet.PacketShowInvisibleBlock;
import re.alwyn974.novacraft.network.proxy.CommonProxy;
import re.alwyn974.novacraft.server.command.CommandShow;

@Mod(modid = NovaCraft.MODID, name = NovaCraft.NAME, version = NovaCraft.VERSION)
public class NovaCraft {
	public static final String MODID = "novacraft";
	public static final String TEXTURE_ID = "novacraft:";
	public static final String VERSION = "%VERSION%";
	public static final String NAME = "NovaCraft Mod";

	@Instance(MODID)
	public static NovaCraft instance;
	@SidedProxy(clientSide = "re.alwyn974.novacraft.network.proxy.ClientProxy", serverSide = "re.alwyn974.novacraft.network.proxy.CommonProxy")
	public static CommonProxy proxy;

	public static CreativeTabs NovaCraftCreativeTabs = new NovaCraftCreativeTabs("novacraft_creative_tab");

	public static Block MicroliteOre, SpinelleOre, GraphiteOre, InsidiumOre, MicroliteBlock, SpinelleBlock,
			GraphiteBlock, InsidiumBlock, AppleHealCrops, BananaCrops, MureCrops, GraphiteCrops, SpinelleCrops,
			MicroliteCrops, CaveBlock, RandomOre, ReinforcedObsidian, StoneStairs, ObsidianStairs, GlowStoneStairs,
			ObsidianSlab, ObsidianDoubleSlab, GlowStoneSlab, GlowStoneDoubleSlab, ObsidianFence, GlowStoneFence,
			IronFence, ObsidianTrapdoor, GraphiteLadder, GraphiteChest, InvisibleBlock, BananaLeaves, BananaSapling;

	public static Block[] HardenedClayStairs = new Block[17], NovacraftSlabs = new Block[17], NovacraftDoubleSlabs = new Block[17];

	public static Potion SlowFall, ViewEffect;

	public static Item Microlite, Spinelle, Insidium, Graphite, MicroliteHelmet, MicroliteChestPlate, MicroliteLeggings,
			MicroliteBoots, SpinelleHelmet, SpinelleChestPlate, SpinelleLeggings, SpinelleBoots, InsidiumHelmet,
			InsidiumChestPlate, InsidiumLeggings, InsidiumBoots, GraphiteHelmet, GraphiteChestPlate, GraphiteLeggings,
			GraphiteBoots, MicroliteSword, MicrolitePickaxe, MicroliteAxe, MicroliteShovel, MicroliteHoe,
			MicroliteBattleHammer, MicroliteBattleAxe, SpinelleSword, SpinellePickaxe, SpinelleAxe, SpinelleShovel,
			SpinelleHoe, SpinelleBattleHammer, SpinelleBattleAxe, InsidiumSword, InsidiumPickaxe, InsidiumAxe,
			InsidiumShovel, InsidiumHoe, InsidiumBattleHammer, InsidiumBattleAxe, GraphiteSword, GraphitePickaxe,
			GraphiteAxe, GraphiteShovel, GraphiteHoe, GraphiteBattleHammer, GraphiteBattleAxe, AppleHeal,
			AppleHealSeeds, Banana, BananaSeeds, Mure, MureSeeds, DiamondBow, InsidiumBow, MicroliteBow, GraphiteBow,
			SpinelleBow, IronStick, Rod, RodOfHeal, RodOfFall, RodOfView, RodOfHaste, Petard, GraphiteShard,
			MicroliteShard, SpinelleShard, GraphiteSeeds, SpinelleSeeds, MicroliteSeeds;

	public static Item[] Keys = new Item[6];
	
	public static ArmorMaterial InsidiumArmor = EnumHelper.addArmorMaterial("InsidiumArmor", 42, new int[] { 6, 8, 10, 6 }, 10);
	public static ArmorMaterial SpinelleArmor = EnumHelper.addArmorMaterial("SpinelleArmor", 39, new int[] { 5, 7, 8, 6 }, 12);
	public static ArmorMaterial MicroliteArmor = EnumHelper.addArmorMaterial("MicroliteArmor", 37, new int[] { 5, 6, 8, 5 }, 13);
	public static ArmorMaterial GraphiteArmor = EnumHelper.addArmorMaterial("GraphiteArmor", 35, new int[] { 4, 6, 8, 4 }, 14);
	
	public static ToolMaterial InsidiumTool = EnumHelper.addToolMaterial("InsidiumTool", 3, 3000, 15.0F, 7.0F, 10);
	public static ToolMaterial SpinelleTool = EnumHelper.addToolMaterial("SpinelleTool", 3, 2500, 13.0F, 5.0F, 12);
	public static ToolMaterial MicroliteTool = EnumHelper.addToolMaterial("MicroliteTool", 3, 2000, 13.0F, 4.5F, 13);
	public static ToolMaterial GraphiteTool = EnumHelper.addToolMaterial("GraphiteTool", 3, 1700, 12.0F, 3.5F, 14);

	public static final Logger LOGGER = LogManager.getLogger("NovaCraft");
	public static SimpleNetworkWrapper network;
	
	static {
		InsidiumTool.setRepairItem(new ItemStack(Insidium));
		SpinelleTool.setRepairItem(new ItemStack(Spinelle));
		MicroliteTool.setRepairItem(new ItemStack(Microlite));
		GraphiteTool.setRepairItem(new ItemStack(Graphite));
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		LOGGER.info("Pre-Init Phase");
		metadataInit(event.getModMetadata());
		// Message Copyright - Début
		LOGGER.info("----------------------------------------------------");
		LOGGER.info("Mod create by Alwyn974 (https://github.com/alwyn974)");
		LOGGER.info("----------------------------------------------------");
		// Message Copyright - Fin
		
		new ArmorRegister();
		new BlockRegister();
		new ItemRegister();
		proxy.registerRender();
		new ToolRegister();
		WorldRegister.mainRegistry();
		new WeaponRegister();
		
		network = NetworkRegistry.INSTANCE.newSimpleChannel("NovaCraft");
		network.registerMessage(PacketShowInvisibleBlock.Handler.class, PacketShowInvisibleBlock.class, 0, Side.CLIENT);
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		LOGGER.info("Init Phase");
		proxy.registerRender();
		new CraftRegister();
		new EventsCommon();
		new PotionEvent();
		new PotionRegister();
		new SeedGenRegister();

		if (event.getSide().isClient()) {
			new EventsClient();
		}
		
		EntityRegistry.registerModEntity(EntityPetard.class, "EntityPetard", 420, instance, 32, 20, false);
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandler());
		GameRegistry.registerTileEntity(TileEntityGraphiteChest.class, MODID + ":tile_entity_graphite_chest");
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		proxy.registerRender();
	}
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent event) {
		event.registerServerCommand(new CommandShow());	
	}

	public void metadataInit(ModMetadata data) {
		data.modId = MODID;
		data.name = NAME;
		data.description = "NovaCraft Mod for server NovaCraft";
		data.version = VERSION;
		data.url = "https://github.com/alwyn974";
		if (!data.authorList.contains("Alwyn974")) {
			data.authorList.clear();
			data.authorList.add(0, "Alwyn974");
		}
		data.credits = "All Credits to Alwyn974 (I'm a fucking genius)";
		data.logoFile = "logo.png";
		LOGGER.info("mcmod.info initialized");
	}

}
