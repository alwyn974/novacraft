/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class NovaCraftCreativeTabs extends CreativeTabs {
	
	public NovaCraftCreativeTabs(String label) {
		super(label);
	}

	@Override
	public Item getTabIconItem() {
		return NovaCraft.Spinelle;
	}

	@Override
	public boolean hasSearchBar() {
		return false;
	}

}
