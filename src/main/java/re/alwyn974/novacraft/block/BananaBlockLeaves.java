/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.block;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockLeaves;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.ColorizerFoliage;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import re.alwyn974.novacraft.NovaCraft;

public class BananaBlockLeaves extends BlockLeaves {

	protected IIcon fastIcon;

	public BananaBlockLeaves(String name) {
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setBlockName(name);
		this.setBlockTextureName(NovaCraft.TEXTURE_ID + name);
	}

	@Override
	public IIcon getIcon(int side, int meta) {
		return isOpaqueCube() ? fastIcon : blockIcon;
	}

	@Override
	public String[] func_150125_e() {
		return null;
	}

	@Override
	public void registerBlockIcons(IIconRegister iconregister) {
		blockIcon = iconregister.registerIcon(this.getTextureName());
		fastIcon = iconregister.registerIcon(this.getTextureName() + "_opaque");
	}

	@Override
	public boolean isOpaqueCube() {
		return Blocks.leaves.isOpaqueCube();
	}

	@Override
	public boolean isLeaves(IBlockAccess world, int x, int y, int z) {
		return true;
	}

	@SideOnly(Side.CLIENT)
	public boolean shouldSideBeRendered(IBlockAccess blockaccess, int x, int y, int z, int side) {
		return !this.isOpaqueCube() ? true : super.shouldSideBeRendered(blockaccess, x, y, z, side);
	}

	@SideOnly(Side.CLIENT)
	public int getBlockColor() {
		return ColorizerFoliage.getFoliageColor(0.5D, 1.0D);
	}

	@Override
	public int getRenderColor(int meta) {
		return ColorizerFoliage.getFoliageColorBasic();
	}

	@Override
	public int quantityDropped(Random random) {
		return random.nextInt(20) == 0 ? 1 : 0;
	}

	@Override
	public Item getItemDropped(int metadata, Random random, int par3) {
		return Item.getItemFromBlock(NovaCraft.BananaSapling);
	}

	@Override
	public void dropBlockAsItemWithChance(World world, int x, int y, int z, int metadata, float chance, int fortune) {
		if (!world.isRemote) {
			if (world.rand.nextInt(20) == 0) {
				Item splingid = this.getItemDropped(metadata, world.rand, fortune);
				this.dropBlockAsItem(world, x, y, z, new ItemStack(splingid, 1, this.damageDropped(metadata)));
			}
		}
	}

}
