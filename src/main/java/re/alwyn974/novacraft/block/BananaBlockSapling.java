/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.block;

import java.util.List;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSapling;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.world.gen.BananaTreeGenerator;

public class BananaBlockSapling extends BlockSapling {

	public BananaBlockSapling(String name) {
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setBlockName(name);
		this.setBlockTextureName(NovaCraft.TEXTURE_ID + name);
		float f = 0.4F;
		this.setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, f * 2.0F, 0.5F + f);
		this.setStepSound(soundTypeGrass);
	}

	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int metadata) {
		return blockIcon;
	}

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iiconRegister) {
		this.blockIcon = iiconRegister.registerIcon(this.getTextureName());
	}

	@Override
	public void func_149878_d(World world, int x, int y, int z, Random random) {
		int l = world.getBlockMetadata(x, y, z) & 3;
		world.setBlockToAir(x, y, z);
		Object obj = new BananaTreeGenerator(true);
		if (!((WorldGenerator) (obj)).generate(world, random, x, y, z)) {
			world.setBlock(x, y, z, this, l, 3);
		}
	}

	@Override
	protected boolean canPlaceBlockOn(Block block) {
		return block == Blocks.sand;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void getSubBlocks(Item item, CreativeTabs tab, List list) {
		list.add(new ItemStack(this, 1 , 0));
	}

}
