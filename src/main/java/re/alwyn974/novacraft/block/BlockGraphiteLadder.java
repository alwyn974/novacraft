/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLadder;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import re.alwyn974.novacraft.NovaCraft;

public class BlockGraphiteLadder extends BlockLadder {
	
	public BlockGraphiteLadder(String name) {
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setBlockName(name);
		this.setBlockTextureName(NovaCraft.MODID + ":" + name);
	}
	
	@Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
		if (entity.onGround || entity.isCollidedVertically)
			return;
		if (entity.motionY >= 0.1) {
			entity.setPosition(entity.posX, entity.posY + 0.05F, entity.posZ);
		} else if (entity.motionY <= -0.1) {
			Block blockUnder = world.getBlock(MathHelper.floor_double(entity.posX), MathHelper.floor_double(entity.posY) - 3, MathHelper.floor_double(entity.posZ));
			if (blockUnder == null || blockUnder == this)
				entity.setPosition(entity.posX, entity.posY - 0.05F, entity.posZ);
		}
	}
}
