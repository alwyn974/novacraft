/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.block;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.util.ForgeDirection;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.network.proxy.ClientProxy;

public class BlockInvisible extends Block {
	
	IIcon[] iconArray;

	public BlockInvisible() {
		super(new MaterialInvisibleBlock());
		this.setBlockUnbreakable();
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setBlockName("invisible_block");
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public IIcon getIcon(int side, int meta) {
		if (ClientProxy.insivisibleBlock)
			return iconArray[0];
		else 
			return iconArray[1];
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister reg) {
		iconArray = new IIcon[2];
		iconArray[0] = reg.registerIcon(NovaCraft.TEXTURE_ID + "invisible_block");
		iconArray[1] = reg.registerIcon(NovaCraft.TEXTURE_ID + "barrier_block");
	}
	
	@Override
	public Item getItemDropped(int metadata, Random rand, int fortune) {
		return null;
	}
	
	@SideOnly(Side.CLIENT)
	@Override	
	public int getRenderColor(int meta) {
		return 0xffffff;
	}
	
	@Override
	public boolean isNormalCube() {
		return true;
	}
	
	@Override
	public int getLightOpacity() {
		return 0;
	}
	
	@Override
	public boolean getCanBlockGrass() {
		return false;
	}
	
	@Override
	public boolean isSideSolid(IBlockAccess world, int x, int y, int z, ForgeDirection side) {
		return true;
	}
	
}
