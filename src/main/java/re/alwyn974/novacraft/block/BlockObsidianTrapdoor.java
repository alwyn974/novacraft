/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.block;

import net.minecraft.block.BlockTrapDoor;
import net.minecraft.block.material.Material;
import re.alwyn974.novacraft.NovaCraft;

public class BlockObsidianTrapdoor extends BlockTrapDoor {

	public BlockObsidianTrapdoor(String name) {
		super(Material.rock);
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setBlockName(name);
		this.setBlockTextureName(NovaCraft.TEXTURE_ID + name);
	}

}
