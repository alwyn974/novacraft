package re.alwyn974.novacraft.block;

import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;

public class MaterialInvisibleBlock extends Material {

	public MaterialInvisibleBlock() {
		super(MapColor.airColor);
		this.setRequiresTool();
	}

	@Override
	public boolean isSolid() {
		return true;
	}

	/**
	 * Will prevent grass from growing on dirt underneath and kill any grass below
	 * it if it returns true
	 */
	@Override
	public boolean getCanBlockGrass() {
		return false;
	}

	/**
	 * Returns if this material is considered solid or not
	 */
	@Override
	public boolean blocksMovement() {
		return true;
	}

	/**
	 * Indicate if the material is opaque
	 */
	@Override
	public boolean isOpaque() {
		return false;
	}

}