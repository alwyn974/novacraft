/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import re.alwyn974.novacraft.NovaCraft;

public class NBlock extends Block {
	
	public NBlock(Material material, String name) {
		super(material);
		this.setHardness(3.0F).setResistance(5.0F);
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setBlockName(name);
		this.setBlockTextureName(NovaCraft.TEXTURE_ID + name);
	}
	
}
