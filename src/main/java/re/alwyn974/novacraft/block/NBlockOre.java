/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.block;

import java.util.Random;

import net.minecraft.block.BlockOre;
import net.minecraft.item.Item;
import re.alwyn974.novacraft.NovaCraft;

public class NBlockOre extends BlockOre {
			
	public NBlockOre(String name, String... infos) {
		this.setHardness(3.0F).setResistance(5.0F).setHarvestLevel("pickaxe", 3);
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setBlockName(name);
		this.setBlockTextureName(NovaCraft.TEXTURE_ID + name);
	}
	
	@Override
	public int quantityDropped(Random r) {
		return 1;
	}
	
	@Override
	public Item getItemDropped(int metadata, Random rand, int fortune) {
		if (this == NovaCraft.RandomOre) {
			int g = rand.nextInt(100);
			if (g <= 55) 
				return NovaCraft.GraphiteShard;
			int m = rand.nextInt(100);
			if (m <= 35) 
				return NovaCraft.MicroliteShard;
			int s = rand.nextInt(100);
			if (s <= 10) 
				return NovaCraft.SpinelleShard;	
		}
		return Item.getItemFromBlock(this);
	}

}
