/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.block;

import java.util.ArrayList;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import re.alwyn974.novacraft.NovaCraft;

public class NBlockSlab extends BlockSlab {

	private boolean hasToptexture = false;
	private boolean hasBottomtexture = false;
	private IIcon top;
	private IIcon bottom;
	private Block singleSlab;

	public NBlockSlab(String name, boolean stacked, Block material) {
		super(stacked, material.getMaterial());
		this.setCreativeTab(stacked ? null : NovaCraft.NovaCraftCreativeTabs);
		this.setBlockName(name);
		this.useNeighborBrightness = (!stacked);
	}

	@Override
	public String func_150002_b(int meta) {
		return getUnlocalizedName();
	}

	public void hasTop(boolean value) {
		this.hasToptexture = value;
	}

	public void hasBottom(boolean value) {
		this.hasBottomtexture = value;
	}

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister reg) {
		if (this.hasToptexture) {
			this.top = reg.registerIcon(getTextureName() + "_top");
		}
		if (this.hasBottomtexture) {
			this.bottom = reg.registerIcon(getTextureName() + "_bottom");
		}
		if ((this.hasToptexture) || (this.hasBottomtexture)) {
			this.blockIcon = reg.registerIcon(getTextureName() + "_side");
		} else {
			this.blockIcon = reg.registerIcon(getTextureName());
		}
	}

	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta) {
		if ((this.hasBottomtexture) && (side == 0)) {
			return this.bottom;
		}
		if ((this.hasToptexture) && (side < 2)) {
			return this.top;
		}
		return this.blockIcon;
	}

	public void setSingleSlab(Block slab) {
		this.singleSlab = slab;
	}

	protected ItemStack createStackedBlock(int meta) {
		if (this.singleSlab != null) {
			return new ItemStack(this.singleSlab, 2, meta & 0x7);
		}
		return new ItemStack(Item.getItemFromBlock(this), 1, meta);
	}

	public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune) {
		ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
		if (this.singleSlab != null) {
			ret.add(new ItemStack(this.singleSlab, 2, metadata & 0x7));
		} else {
			ret.add(new ItemStack(Item.getItemFromBlock(this), 1, metadata));
		}
		return ret;
	}

}
