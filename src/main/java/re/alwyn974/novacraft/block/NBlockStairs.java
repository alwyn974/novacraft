/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.block;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.BlockStairs;
import net.minecraft.item.ItemStack;
import re.alwyn974.novacraft.NovaCraft;

public class NBlockStairs extends BlockStairs {
	
	Block block;

	public NBlockStairs(Block block, String name, int meta) {
		super(block, meta);
		this.block = block;
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setBlockName(name);
		this.useNeighborBrightness = true;
		GameRegistry.registerBlock(this, name);
		GameRegistry.addRecipe(new ItemStack(this, 1, meta), new Object[] {"#  ", "## ", "###", '#', new ItemStack(block, 1, meta)});
	}

	public int damageDropped(int meta) {
		return meta;
	}

}
