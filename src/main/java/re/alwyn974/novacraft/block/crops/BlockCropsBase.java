/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.block.crops;

import java.util.ArrayList;

import net.minecraft.block.BlockCrops;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class BlockCropsBase extends BlockCrops {
	
	private String name;
	
	public BlockCropsBase(String name) {
		this.name = name;
		this.setBlockName(name);
	}

	@Override
	public int damageDropped(int metadata) {
		return 0;
	}
	
	@Override
	public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune) {
		ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
		
		/*1 SEED*/
		if (metadata >= 7) {
			for (int i = 0; i < 1 + fortune; ++i) {
				if (world.rand.nextInt(100) <= 60) {
					ret.add(new ItemStack(this.func_149866_i(), 1, 0));
				}
			}
		}
		
		/*1 DROP */
		if ((metadata >= 7)) {
			for (int i = 0; i < 1 + fortune; i++) {
				if (world.rand.nextInt(100) <= 10) {
					ret.add(new ItemStack(func_149865_P(), 1, 0));
				}
			}
		}
		
		/*1 SEED AND 1 DROP */
		if ((metadata >= 7)) {
			for (int i = 0; i < 1 + fortune; i++) {
				if (world.rand.nextInt(100) <= 20) {
					ret.add(new ItemStack(func_149866_i(), 1, 0));
					ret.add(new ItemStack(func_149865_P(), 1, 0));
				}
			}
		}
		
		/*2 SEED AND 1 DROP */
		if ((metadata >= 7)) {
			for (int i = 0; i < 1 + fortune; i++) {
				if (world.rand.nextInt(100) <= 10) {
					ret.add(new ItemStack(func_149866_i(), 2, 0));
					ret.add(new ItemStack(func_149865_P(), 1, 0));
				}
			}
		}
		
		return ret;
	}

	public String getName() {
		return name;
	}

}
