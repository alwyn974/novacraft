/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.block.crops;

import java.util.ArrayList;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class BlockCropsBaseMineral extends BlockCropsBase {

	public BlockCropsBaseMineral(String name) {
		super(name);
	}
	
	@Override
	public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune) {
		ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
		
		/*1 SEED*/
		if (metadata >= 7) {
			for (int i = 0; i < 1 + fortune; ++i) {
				if (world.rand.nextInt(100) <= 50) {
					ret.add(new ItemStack(this.func_149866_i(), 1, 0));
				}
			}
		}
		
		/*2 SEED */
		if ((metadata >= 7)) {
			for (int i = 0; i < 1 + fortune; i++) {
				if (world.rand.nextInt(100) <= 10) {
					ret.add(new ItemStack(func_149866_i(), 2, 0));
				}
			}
		}
		
		/*2 SEED AND 1 DROP */
		if ((metadata >= 7)) {
			for (int i = 0; i < 1 + fortune; i++) {
				if (world.rand.nextInt(100) <= 30) {
					ret.add(new ItemStack(func_149866_i(), 2, 0));
					ret.add(new ItemStack(func_149865_P(), 1, 0));
				}
			}
		}
		
		/*2 SEED AND 4 DROP */
		if ((metadata >= 7)) {
			for (int i = 0; i < 1 + fortune; i++) {
				if (world.rand.nextInt(100) <= 10) {
					ret.add(new ItemStack(func_149866_i(), 2, 0));
					ret.add(new ItemStack(func_149865_P(), 4, 0));
				}
			}
		}
		
		return ret;
	}
}
