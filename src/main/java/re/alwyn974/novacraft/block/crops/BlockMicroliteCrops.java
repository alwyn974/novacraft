/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.block.crops;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import re.alwyn974.novacraft.NovaCraft;

public class BlockMicroliteCrops extends BlockCropsBaseMineral {
	
	public static String[] type = new String[8];
	private IIcon[] IconArray;

	public BlockMicroliteCrops() {
		super("microlite_crops");
		for (int i = 0; i < 8; i++) {
			type[i] = this.getName() + "_stage_" + i;
		}
	}
	
	@Override
	protected Item func_149866_i() {
		return NovaCraft.MicroliteSeeds;
	}

	@Override
	protected Item func_149865_P() {
		return NovaCraft.MicroliteShard;
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public IIcon getIcon(int side, int meta) {
		if (meta < 7) {
			if (meta == 6) {
				meta = 5;
			}
			return this.IconArray[meta >> 1];
		} else {
			return this.IconArray[3];
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister iconregister) {
		IconArray = new IIcon[type.length];

		for (int i = 0; i < type.length; i++) {
			IconArray[i] = iconregister.registerIcon(NovaCraft.TEXTURE_ID + type[i]);
		}
	}

}
