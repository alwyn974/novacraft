/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.block.entity;

import java.util.Iterator;
import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraftforge.common.util.Constants;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.common.gui.GraphiteChestContainer;

public class TileEntityGraphiteChest extends TileEntity implements IInventory {

	private int ticksSinceSync = -1;
	public float prevLidAngle;
	public float lidAngle;
	private int numUsingPlayers;
	private ItemStack[] contents = new ItemStack[54];
	private boolean inventoryTouched;
	private String customName;

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);

		if (compound.hasKey("CustomName", Constants.NBT.TAG_STRING))
			this.customName = compound.getString("CustomName");

		NBTTagList nbttaglist = compound.getTagList("Items", Constants.NBT.TAG_COMPOUND);

		this.contents = new ItemStack[this.getSizeInventory()];
		for (int i = 0; i < nbttaglist.tagCount(); ++i) {
			NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
			int j = nbttagcompound1.getByte("Slot") & 255;

			if (j >= 0 && j < this.contents.length) {
				this.contents[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);

			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound compound) {
		super.writeToNBT(compound);
		if (this.hasCustomInventoryName())
			compound.setString("CustomName", this.customName);

		NBTTagList nbttaglist = new NBTTagList();
		for (int i = 0; i < this.contents.length; ++i) {
			if (this.contents[i] != null) {
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.contents[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}
		compound.setTag("Items", nbttaglist);
	}

	public ItemStack[] getContents() {
		return contents;
	}

	@Override
	public int getSizeInventory() {
		return this.contents.length;
	}

	@Override
	public ItemStack getStackInSlot(int slotIndex) {
		inventoryTouched = true;
		return this.contents[slotIndex];
	}

	@Override
	public ItemStack decrStackSize(int slotIndex, int amount) {
		if (this.contents[slotIndex] != null) {
			ItemStack itemstack;

			if (this.contents[slotIndex].stackSize <= amount) {
				itemstack = this.contents[slotIndex];
				this.contents[slotIndex] = null;
				this.markDirty();
				return itemstack;
			} else {
				itemstack = this.contents[slotIndex].splitStack(amount);

				if (this.contents[slotIndex].stackSize == 0) {
					this.contents[slotIndex] = null;
				}
				this.markDirty();
				return itemstack;
			}
		} else
			return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slotIndex) {
		if (this.contents[slotIndex] != null) {
			ItemStack itemstack = this.contents[slotIndex];
			this.contents[slotIndex] = null;
			return itemstack;
		} else
			return null;

	}

	@Override
	public void setInventorySlotContents(int slotIndex, ItemStack stack) {
		this.contents[slotIndex] = stack;

		if (stack != null && stack.stackSize > this.getInventoryStackLimit())
			stack.stackSize = this.getInventoryStackLimit();

		this.markDirty();
	}

	@Override
	public String getInventoryName() {
		return this.hasCustomInventoryName() ? this.customName : "tile.inventory_graphite_chest";
	}

	public void setCustomName(String customName) {
		this.customName = customName;
	}

	@Override
	public boolean hasCustomInventoryName() {
		return this.customName != null && this.customName.length() > 0;
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false
				: player.getDistanceSq((double) this.xCoord + 0.5D, (double) this.yCoord + 0.5D,
						(double) this.zCoord + 0.5D) <= 64.0D;
	}

	@Override
	public void openInventory() {
		if (worldObj == null)
			return;
		numUsingPlayers++;
		worldObj.addBlockEvent(xCoord, yCoord, zCoord, NovaCraft.GraphiteChest, 1, numUsingPlayers);
	}

	@Override
	public void closeInventory() {
		if (worldObj == null)
			return;
		numUsingPlayers--;
		worldObj.addBlockEvent(xCoord, yCoord, zCoord, NovaCraft.GraphiteChest, 1, numUsingPlayers);
	}

	@Override
	public boolean isItemValidForSlot(int p_94041_1_, ItemStack p_94041_2_) {
		return true;
	}

	@Override
	public void updateEntity() {
		super.updateEntity();
		// Resynchronize clients with the server state
		if (worldObj != null && !this.worldObj.isRemote && this.numUsingPlayers != 0
				&& (this.ticksSinceSync + this.xCoord + this.yCoord + this.zCoord) % 200 == 0) {
			this.numUsingPlayers = 0;
			float f = 5.0F;
			@SuppressWarnings("unchecked")
			List<EntityPlayer> list = this.worldObj.getEntitiesWithinAABB(EntityPlayer.class,
					AxisAlignedBB.getBoundingBox((double) ((float) this.xCoord - f), (double) ((float) this.yCoord - f),
							(double) ((float) this.zCoord - f), (double) ((float) (this.xCoord + 1) + f),
							(double) ((float) (this.yCoord + 1) + f), (double) ((float) (this.zCoord + 1) + f)));
			Iterator<EntityPlayer> iterator = list.iterator();

			while (iterator.hasNext()) {
				EntityPlayer player = iterator.next();

				if (player.openContainer instanceof GraphiteChestContainer) {
					++this.numUsingPlayers;
				}
			}
		}

		if (worldObj != null && !worldObj.isRemote && ticksSinceSync < 0) {
			worldObj.addBlockEvent(xCoord, yCoord, zCoord, NovaCraft.GraphiteChest, 3,
					((numUsingPlayers << 3) & 0xF8) | (getBlockMetadata() & 0x7));
		}

		if (!worldObj.isRemote && inventoryTouched) {
			inventoryTouched = false;
		}

		this.ticksSinceSync++;
		prevLidAngle = lidAngle;
		float f = 0.1F;
		if (numUsingPlayers > 0 && lidAngle == 0.0F) {
			double d = (double) xCoord + 0.5D;
			double d1 = (double) zCoord + 0.5D;
			worldObj.playSoundEffect(d, (double) yCoord + 0.5D, d1, "random.chestopen", 0.5F,
					worldObj.rand.nextFloat() * 0.1F + 0.9F);
		}
		if (numUsingPlayers == 0 && lidAngle > 0.0F || numUsingPlayers > 0 && lidAngle < 1.0F) {
			float f1 = lidAngle;
			if (numUsingPlayers > 0) {
				lidAngle += f;
			} else {
				lidAngle -= f;
			}
			if (lidAngle > 1.0F) {
				lidAngle = 1.0F;
			}
			float f2 = 0.5F;
			if (lidAngle < f2 && f1 >= f2) {
				double d2 = (double) xCoord + 0.5D;
				double d3 = (double) zCoord + 0.5D;
				worldObj.playSoundEffect(d2, (double) yCoord + 0.5D, d3, "random.chestclosed", 0.5F,
						worldObj.rand.nextFloat() * 0.1F + 0.9F);
			}
			if (lidAngle < 0.0F) {
				lidAngle = 0.0F;
			}
		}
	}
}
