/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.client.events;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.item.ItemBow;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.client.event.FOVUpdateEvent;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.common.MinecraftForge;
import re.alwyn974.novacraft.client.gui.GuiLogin;
import re.alwyn974.novacraft.client.gui.GuiRegister;
import re.alwyn974.novacraft.client.gui.NovaCraftGuiIngameMenu;

@SideOnly(Side.CLIENT)
public class EventsClient {

	public EventsClient() {
		FMLCommonHandler.instance().bus().register(this);
		MinecraftForge.EVENT_BUS.register(this);
	}

	@SubscribeEvent
	public void onGuiOpen(GuiOpenEvent e) {
		if (e.gui != null) {
			if (e.gui instanceof GuiIngameMenu)
				e.gui = new NovaCraftGuiIngameMenu();
		}
	}


	boolean alreadyLoginOpen = false;
	boolean alreadyRegisterOpen = false;

	@SubscribeEvent
	public void onChatReceivedEvent(ClientChatReceivedEvent e) {
		System.out.println(e.message.getFormattedText());
		if (e.message.getFormattedText().contains("§8[§r§4NovaCraft§r§8] §r§7Pour vous inscrire, utilisez \"/register <MotDePasse> <ConfirmerMotDePasse>\"") && !alreadyRegisterOpen) {
			Minecraft.getMinecraft().displayGuiScreen(new GuiRegister());
			alreadyRegisterOpen = true;
		} else if (e.message.getFormattedText().contains("§8[§r§4NovaCraft§r§8] §r§7Pour vous connecter, utilisez \"/login <MotDePasse>\"") && !alreadyLoginOpen) {
			Minecraft.getMinecraft().displayGuiScreen(new GuiLogin());
			alreadyLoginOpen = true;
		} else if (e.message.getFormattedText().contains("§7Connexion effectuée !") || e.message.getFormattedText().contains("§7Inscription effectué !"))
			Minecraft.getMinecraft().displayGuiScreen((GuiScreen) null);
		else if (!e.message.getFormattedText().contains("§8[§r§4NovaCraft§r§8] §r§7Pour vous inscrire, utilisez \"/register <MotDePasse> <ConfirmerMotDePasse>\""))
			alreadyRegisterOpen = false;
		else if (!e.message.getFormattedText().contains("§8[§r§4NovaCraft§r§8] §r§7Pour vous connecter, utilisez \"/login <MotDePasse>\""))
			alreadyLoginOpen = false;
	}

	@SubscribeEvent
	public void onFOVUpdate(FOVUpdateEvent e) {
		if (e.entity.isUsingItem() && e.entity.getItemInUse().getItem() instanceof ItemBow) {
			float f = 1.0F;
			int i = e.entity.getItemInUseDuration();
			float f1 = (float) i / 20.0F;
			if (f1 > 1.0F)
				f1 = 1.0F;
			else
				f1 *= f1;
			f *= 1.0F - f1 * 0.15F;
			e.newfov = f;
		}
	}

}