/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.client.gui;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import re.alwyn974.novacraft.block.entity.TileEntityGraphiteChest;
import re.alwyn974.novacraft.common.gui.GraphiteChestContainer;

public class GuiGraphiteChest extends GuiContainer {

	private static final ResourceLocation texture = new ResourceLocation("textures/gui/container/generic_54.png");
	private TileEntityGraphiteChest tileEntity;
	private IInventory playerInv;

	public GuiGraphiteChest(TileEntityGraphiteChest tile, InventoryPlayer inventory) {
		super(new GraphiteChestContainer(tile, inventory));
		this.tileEntity = tile;
		this.playerInv = inventory;
		this.allowUserInput = false;
		this.ySize = 222;
		this.xSize = 176;
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int x, int y) {
		String tileName = this.tileEntity.hasCustomInventoryName() ? this.tileEntity.getInventoryName() : I18n.format(this.tileEntity.getInventoryName());
		this.fontRendererObj.drawString(tileName, 8, 6, 4210752);
		String invName = this.playerInv.hasCustomInventoryName() ? this.playerInv.getInventoryName() : I18n.format(this.playerInv.getInventoryName());
		this.fontRendererObj.drawString(invName, 8, this.ySize - 94, 4210752);
	}
	
	@Override
    protected void drawGuiContainerBackgroundLayer(float partialRenderTick, int x, int y) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(texture);
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
    }

}
