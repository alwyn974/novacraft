/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.client.gui;

import java.awt.Color;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiYesNoCallback;
import net.minecraft.util.ResourceLocation;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.client.gui.buttons.GuiButtonJoin;
import re.alwyn974.novacraft.client.gui.buttons.GuiButtonLeave;

@SideOnly(Side.CLIENT)
public class GuiLogin extends GuiScreen implements GuiYesNoCallback {
	
	private final ResourceLocation background = new ResourceLocation(NovaCraft.MODID, "textures/gui/login-novacraft.png");
	private NGuiTextField inputField;
	
	public GuiLogin() {}
	
	@SuppressWarnings("unchecked")
	@Override
	public void initGui() {
		this.buttonList.clear();
		Keyboard.enableRepeatEvents(true);
		this.inputField = new NGuiTextField(this.fontRendererObj, this.width / 2 - 52, this.height / 2 - 25, 100, 30);
		this.inputField.setEnableBackgroundDrawing(false);
		this.inputField.setLogin(true);
		this.buttonList.add(new GuiButtonJoin(1, this.width / 2 - 30, this.height / 2 + 38));
		this.buttonList.add(new GuiButtonLeave(2, this.width / 2 + 50, this.height / 2 - 95));
		
		super.initGui();
	}
	
	@Override
	public void drawScreen(int x, int y, float partialTicks) {
		this.drawDefaultBackground();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(background);
		int xSize = 176;
		int ySize = 238;
		int k = (this.width - xSize) / 2;
		int l = (this.height - ySize) / 2;
		func_146110_a(k + 4, l, 0, 0, 238, 238, 238, 238);
		
		this.drawCenteredString(fontRendererObj, this.inputField.getForcePassword(), this.width / 2, this.height / 2, Color.white.getRGB());
		this.inputField.drawTextBox();
		super.drawScreen(x, y, partialTicks);
	}
	
	@Override
	protected void mouseClicked(int x, int y, int mouseButton) {
		super.mouseClicked(x, y, mouseButton);
		this.inputField.mouseClicked(x, y, mouseButton);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) {
		this.inputField.textboxKeyTyped(typedChar, keyCode);
		
		if (keyCode == 28)
			Minecraft.getMinecraft().thePlayer.sendChatMessage("/login " + inputField.getText());
		
		super.keyTyped(typedChar, keyCode);
	}
	
	@Override
	protected void actionPerformed(GuiButton button) {
		int id = button.id;
		switch(id) {
		case 1:	
			Minecraft.getMinecraft().thePlayer.sendChatMessage("/login " + inputField.getText());
			break;
		case 2:
			 this.mc.displayGuiScreen((GuiScreen)null);
             this.mc.setIngameFocus();
			break;
		}
		super.actionPerformed(button);
	}
	
	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}
}
