/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.client.gui;

import java.awt.Color;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiYesNoCallback;
import net.minecraft.util.ResourceLocation;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.client.gui.buttons.GuiButtonJoin;
import re.alwyn974.novacraft.client.gui.buttons.GuiButtonLeave;

@SideOnly(Side.CLIENT)
public class GuiRegister extends GuiScreen implements GuiYesNoCallback {
	
	private final ResourceLocation background = new ResourceLocation(NovaCraft.MODID, "textures/gui/register-novacraft.png");
	private NGuiTextField pass1Field;
	private NGuiTextField pass2Field;
	private String same = "";
	public GuiRegister() {}
	
	@SuppressWarnings("unchecked")
	@Override
	public void initGui() {
		this.buttonList.clear();
		Keyboard.enableRepeatEvents(true);
		this.pass1Field = new NGuiTextField(this.fontRendererObj, this.width / 2 - 50, this.height / 2 - 25, 100, 30);
		this.pass1Field.setEnableBackgroundDrawing(false);
		this.pass1Field.setLogin(true);
		this.pass2Field = new NGuiTextField(this.fontRendererObj, this.width / 2 - 52, this.height / 2 + 34, 100, 30);
		this.pass2Field.setEnableBackgroundDrawing(false);
		this.pass2Field.setLogin(true);
		this.buttonList.add(new GuiButtonJoin(1, this.width / 2 - 30, this.height / 2 + 66));
		this.buttonList.add(new GuiButtonLeave(2, this.width / 2 + 50, this.height / 2 - 95));
		
		super.initGui();
	}
	
	@Override
	public void drawScreen(int x, int y, float partialTicks) {
		this.drawDefaultBackground();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(background);
		int xSize = 176;
		int ySize = 238;
		int k = (this.width - xSize) / 2;
		int l = (this.height - ySize) / 2;
		func_146110_a(k + 4, l, 0, 0, 238, 238, 240, 238);
		
		this.drawCenteredString(fontRendererObj, this.pass1Field.getForcePassword(), this.width / 2, this.height / 2 - 7, Color.white.getRGB());
		this.drawCenteredString(fontRendererObj, same, this.width / 2, this.height / 2 + 54, Color.white.getRGB());
		this.pass1Field.drawTextBox();
		this.pass2Field.drawTextBox();

		super.drawScreen(x, y, partialTicks);
	}
	
	@Override
	public void updateScreen() {
		if (this.pass1Field.getForcePassword().length() > 1) {
			if (pass1Field.getText().equals(pass2Field.getText())) 
				same = "§aMot de passe identique";
			else 
				same = "§cMot de passe différent";
		} 
		super.updateScreen();
	}
	
	@Override
	protected void mouseClicked(int x, int y, int mouseButton) {
		super.mouseClicked(x, y, mouseButton);
		this.pass1Field.mouseClicked(x, y, mouseButton);
		this.pass2Field.mouseClicked(x, y, mouseButton);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) {
		this.pass1Field.textboxKeyTyped(typedChar, keyCode);
		this.pass2Field.textboxKeyTyped(typedChar, keyCode);
		
		if (keyCode == 28)
			Minecraft.getMinecraft().thePlayer.sendChatMessage("/register " + pass1Field.getText() + " " + pass2Field.getText());
		
		super.keyTyped(typedChar, keyCode);
	}
	
	@Override
	protected void actionPerformed(GuiButton button) {
		int id = button.id;
		switch(id) {
		case 1:	
			Minecraft.getMinecraft().thePlayer.sendChatMessage("/register " + pass1Field.getText() + " " + pass2Field.getText());
			break;
		case 2:
			 this.mc.displayGuiScreen((GuiScreen)null);
             this.mc.setIngameFocus();
			break;
		}
		super.actionPerformed(button);
	}
	
	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}
}
