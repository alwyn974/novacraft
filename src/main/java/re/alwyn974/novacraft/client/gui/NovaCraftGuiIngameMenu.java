/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.client.gui;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.achievement.GuiAchievements;
import net.minecraft.client.gui.achievement.GuiStats;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.player.EntityPlayer;

public class NovaCraftGuiIngameMenu extends GuiScreen {
	
	private GuiButton quit;
	private ScaledResolution res;

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@SuppressWarnings("unchecked")
	public void initGui() {
		this.buttonList.clear();
		res = new ScaledResolution(this.mc, this.mc.displayWidth, this.mc.displayHeight);
		byte i = -16;
		
		this.buttonList.add(new GuiButton(0, this.width / 2 - 200, this.height / 4 + 96 + i, 98, 20, "§aOptions"));
		this.buttonList.add(quit = new GuiButton(1, this.width / 2 - 100, this.height / 4 + 160 + i, "§3Quitter NovaCraft"));
		this.buttonList.add(new GuiButton(2, this.width / 2 - 200, this.height / 4 + 66 + i, 98, 20, "§9Site"));
		this.buttonList.add(new GuiButton(3, this.width / 2 + 102, this.height / 4 + 66 + i, 98, 20, "§5Wiki"));
		this.buttonList.add(new GuiButton(4, this.width / 2 - 100, this.height / 4 + 130 + i, "§bRevenir en jeu"));
		this.buttonList.add(new GuiButton(5, this.width / 2 - 180, this.height / 4 + 28 + i, 98, 20, "§3Achivements"));
		this.buttonList.add(new GuiButton(6, this.width / 2 + 82, this.height / 4 + 28 + i, 98, 20, "§cStatistiques"));
		this.buttonList.add(new GuiButton(7, this.width / 2 + 102, this.height / 4 + 96 + i, 98, 20, "§eRadio"));
		
		if (this.mc.isIntegratedServerRunning()) 
			quit.displayString = "§3Sauvegarder et quitter";
	}

	protected void actionPerformed(GuiButton button) {
		switch (button.id) {
		case 0:
			this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
			break;
		case 1:
			button.enabled = false;
			this.mc.theWorld.sendQuittingDisconnectingPacket();
			this.mc.loadWorld((WorldClient) null);
			this.mc.displayGuiScreen(new GuiMainMenu());
			break;
		case 2:
			browse("https://novacraft-mc.fr/");
			break;
		case 3:
			browse("https://novacraft-mc.fr/wiki");
			break;
		case 4:
			this.mc.displayGuiScreen((GuiScreen) null);
			this.mc.setIngameFocus();
			break;
		case 5:
			this.mc.displayGuiScreen(new GuiAchievements(this, this.mc.thePlayer.getStatFileWriter()));
			break;
		case 6:
			this.mc.displayGuiScreen(new GuiStats(this, this.mc.thePlayer.getStatFileWriter()));
			break;

		}
	}

	public void browse(String url) {
		if (Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().browse(new URI(url));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
	}

	public void drawPlayer(int x, int y, int scale, float mouseX, float mouseY, EntityPlayer player) {

		FontRenderer fontrenderer = fontRendererObj;
		String name = "§f§k-§9 " + player.getDisplayName() + " §f§k-";
		fontrenderer.drawStringWithShadow(name, x - fontRendererObj.getStringWidth(name) / 2, y - 90, 0);
		
		GL11.glEnable(GL11.GL_COLOR_MATERIAL);
		GL11.glPushMatrix();
		GL11.glTranslatef((float) x, (float) y, 50.0F);
		GL11.glScalef(-scale, scale, scale);
		GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
		float var6 = player.renderYawOffset;
		float var7 = player.rotationYaw;
		float var8 = player.rotationPitch;
		float var9 = player.prevRotationYawHead;
		float var10 = player.rotationYawHead;
		GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
		RenderHelper.enableStandardItemLighting();
		GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
		player.renderYawOffset = (float) Math.atan((double) (mouseX / 40.0F)) * 20.0F;
		player.rotationYaw = (float) Math.atan((double) (mouseX / 40.0F)) * 40.0F;
		player.rotationPitch = -((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F;
		player.rotationYawHead = player.rotationYaw;
		player.prevRotationYawHead = player.rotationYaw;
		GL11.glTranslatef(0.0F, player.yOffset, 0.0F);
		RenderManager.instance.playerViewY = 180.0F;
		RenderManager.instance.renderEntityWithPosYaw(player, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
		player.renderYawOffset = var6;
		player.rotationYaw = var7;
		player.rotationPitch = var8;
		player.prevRotationYawHead = var9;
		player.rotationYawHead = var10;
		GL11.glPopMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);

	}

	private int getScaledValue(int value) {
		if (this.width == 960) 
			return 40;
		else if (this.width > 854) 
			return 0;
		else if (this.height == 480) 
			return 40;
		
		boolean sW = this.width <= 427 || this.width == 640 || this.width == 854;
		return (sW ? value : value * 2) / res.getScaleFactor();
	}

	private int getValue(int value) {
		boolean sW = this.width <= 427 || this.width == 854;
		return (sW ? value : value * 2) / res.getScaleFactor();
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	public void drawScreen(int x, int y, float partialsTick) {
		this.drawDefaultBackground();
		drawPlayer(this.width / 2, height / 3 + getScaledValue(120), 35, ((this.width - getValue(120)) / 2 + height / 8) - x, ((this.height - getValue(170)) / 2) - y, this.mc.thePlayer);
		this.drawCenteredString(this.fontRendererObj, "§5- NovaCraft -", this.width / 2, 40, 16777215);

		super.drawScreen(x, y, partialsTick);
	}
	
}