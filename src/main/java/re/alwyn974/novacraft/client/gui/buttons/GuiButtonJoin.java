/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.client.gui.buttons;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;
import re.alwyn974.novacraft.NovaCraft;

public class GuiButtonJoin extends GuiButton {
	
	private final ResourceLocation join = new ResourceLocation(NovaCraft.MODID, "textures/gui/buttons/join.png");

	public GuiButtonJoin(int id, int x, int y) {
		super(id, x, y, 117, 38, "");
	}
	
	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY) {
		if (this.visible) {
			boolean mouseHover = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
			if (mouseHover) {
				GL11.glEnable(GL11.GL_BLEND);
	            OpenGlHelper.glBlendFunc(770, 771, 1, 0);
	            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				mc.getTextureManager().bindTexture(join);
			} else {
				mc.getTextureManager().bindTexture(join);
			}
			
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			func_146110_a(this.xPosition, this.yPosition, 0, 0, 62, 40, 62, 40);
		}
	}

}
