/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.client.gui.buttons;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;
import re.alwyn974.novacraft.NovaCraft;

public class GuiButtonLeave extends GuiButton {
	
	private final ResourceLocation leave = new ResourceLocation(NovaCraft.MODID, "textures/gui/buttons/close.png");

	public GuiButtonLeave(int id, int x, int y) {
		super(id, x, y, 14, 19, "");
	}
	
	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY) {
		if (this.visible) {
			boolean mouseHover = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
			if (mouseHover) {
				GL11.glEnable(GL11.GL_BLEND);
	            OpenGlHelper.glBlendFunc(770, 771, 1, 0);
	            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				mc.getTextureManager().bindTexture(leave);
			} else {
				mc.getTextureManager().bindTexture(leave);
			}
			
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			func_146110_a(this.xPosition, this.yPosition, 0, 0, 13, 13, 14, 13);
		}
	}
}
