/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.client.render;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.world.IBlockAccess;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.block.entity.TileEntityGraphiteChest;
import re.alwyn974.novacraft.network.proxy.ClientProxy;

public class GraphiteChestInventoryRender implements ISimpleBlockRenderingHandler {

	private TileEntityGraphiteChest tileEntity = new TileEntityGraphiteChest();
	
	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer) {
		if (block == NovaCraft.GraphiteChest && metadata == 0) {
			GL11.glPushMatrix();
			GL11.glRotatef(90F, 0.0F, 1.0F, 0.0F);
            GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
			Minecraft.getMinecraft().getTextureManager().bindTexture(TileEntityGraphiteChestRenderer.texture);
			TileEntityRendererDispatcher.instance.renderTileEntityAt(tileEntity, 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
		return false;
	}

	@Override
	public boolean shouldRender3DInInventory(int modelId) {
		return true;
	}

	@Override
	public int getRenderId() {
		return ClientProxy.graphiteChestRenderId;
	}

}
