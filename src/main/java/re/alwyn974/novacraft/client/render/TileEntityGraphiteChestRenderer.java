/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.client.render;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import net.minecraft.client.model.ModelChest;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.block.entity.TileEntityGraphiteChest;

public class TileEntityGraphiteChestRenderer extends TileEntitySpecialRenderer {

	public static final ResourceLocation texture = new ResourceLocation(NovaCraft.MODID, "textures/blocks/graphite_chest.png");
	public static ModelChest model;

	public TileEntityGraphiteChestRenderer() {
		model = new ModelChest();
		this.func_147497_a(TileEntityRendererDispatcher.instance);
	}

	public void render(TileEntityGraphiteChest tile, double x, double y, double z, float partialTick) {
		if (tile == null) 
			return;
		int facing = 0;
		if (tile != null && tile.hasWorldObj()) 
			facing = tile.getBlockMetadata();
		bindTexture(texture);
		GL11.glPushMatrix();
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glTranslatef((float) x, (float) y + 1.0F, (float) z + 1.0F);
		GL11.glScalef(1.0F, -1F, -1F);
		GL11.glTranslatef(0.5F, 0.5F, 0.5F);
		
		int k = 0;
		if (facing == 0) {
			k = 0;
		}
		if (facing == 2) {
			k = 180;
		}
		if (facing == 3) {
			k = -90;
		}
		if (facing == 1) {
			k = 90;
		}
		GL11.glRotatef(k, 0.0F, 1.0F, 0.0F);
		GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
		float lidangle = tile.prevLidAngle + (tile.lidAngle - tile.prevLidAngle) * partialTick;
		lidangle = 1.0F - lidangle;
		lidangle = 1.0F - lidangle * lidangle * lidangle;
		model.chestLid.rotateAngleX = (float) -((lidangle * Math.PI) / 2.0F);
		model.renderAll();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
	}

	@Override
	public void renderTileEntityAt(TileEntity tileentity, double x, double y, double z, float partialTick) {
		render((TileEntityGraphiteChest) tileentity, x, y, z, partialTick);
	}

}