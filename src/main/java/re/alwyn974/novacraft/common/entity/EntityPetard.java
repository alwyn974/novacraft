/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.entity;

import cpw.mods.fml.common.registry.IEntityAdditionalSpawnData;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityPetard extends EntityThrowable implements IEntityAdditionalSpawnData {

	private int fuseTime = 80;
	private boolean	extinguished;

	public EntityPetard(World world) {
		super(world);
		this.extinguished = false;
	}

	public EntityPetard(World world, EntityLivingBase thrower) {
		super(world, thrower);
		this.extinguished = false;
	}

	@Override
	protected void onImpact(MovingObjectPosition mop) {
		this.motionX = 0;
		this.motionY = 0;
		this.motionZ = 0;
	}

	@Override
	public void onUpdate() {
		super.onUpdate();
		
		if (isInWater() && !extinguished) {
			this.extinguished = true;
			worldObj.playSoundAtEntity(this, "random.fizz", 1.0F, 1.2F / (rand.nextFloat() * 0.2F + 0.9F));
			for (int i = 0; i < 8; i++) {
				float f = 0.25F;
				worldObj.spawnParticle("explode", posX - motionX * f, posY - motionY * f, posZ - motionZ * f, motionX, motionY, motionZ);
			}
		}

		if (this.fuseTime-- <= 0) {
			detonate();
			setDead();
		} else if (fuseTime > 0) {
			worldObj.spawnParticle("smoke", posX, posY, posZ, 0.0D, 0.0D, 0.0D);
		}
	}
	
	private void detonate() {
		if (!worldObj.isRemote) {
			if (extinguished)
				setDead();
			else
				worldObj.createExplosion(this, posX, posY, posZ, 4.0F, true);
		}
	}

	@Override
	public void writeSpawnData(ByteBuf buffer) {
		buffer.writeInt(this.fuseTime);
		buffer.writeDouble(this.motionX);
		buffer.writeDouble(this.motionY);
		buffer.writeDouble(this.motionZ);
	}

	@Override
	public void readSpawnData(ByteBuf additionalData) {
		this.fuseTime = additionalData.readInt();
		this.motionX = additionalData.readDouble();
		this.motionY = additionalData.readDouble();
		this.motionZ = additionalData.readDouble();
	}	

}
