/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.entity;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.potion.Potion;
import net.minecraft.world.World;

public class NEntityArrow extends EntityArrow {
	
	private Potion[] effects;
	
	public NEntityArrow(World world, EntityLivingBase entity, float power, Potion... effects) {
		super(world, entity, power);
		this.effects = effects;
	}

	public Potion[] getEffects() {
		return effects;
	}
	
}
