/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import re.alwyn974.novacraft.block.entity.TileEntityGraphiteChest;

public class GraphiteChestContainer extends Container {

	private final TileEntityGraphiteChest tileEntity;

	public GraphiteChestContainer(TileEntityGraphiteChest tileEntity, InventoryPlayer inventory) {
		this.tileEntity = tileEntity;
		tileEntity.openInventory();
		for (int i = 0; i < 6; ++i) {
			for (int j = 0; j < 9; ++j) {
				this.addSlotToContainer(new Slot(tileEntity, j + i * 9, 8 + j * 18, 18 + i * 18));
			}
		}
		this.bindPlayerInventory(inventory);
	}

	private void bindPlayerInventory(InventoryPlayer inventory) {
		int i;
		for (i = 0; i < 3; ++i) {
			for (int j = 0; j < 9; ++j) {
				this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 8 + j * 18, 140 + i * 18));
			}
		}

		for (i = 0; i < 9; ++i) {
			this.addSlotToContainer(new Slot(inventory, i, 8 + i * 18, 198));
		}
	}

	public ItemStack transferStackInSlot(EntityPlayer player, int slotIndex) {
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(slotIndex);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (slotIndex < this.tileEntity.getSizeInventory()) {
				if (!this.mergeItemStack(itemstack1, this.tileEntity.getSizeInventory(), this.inventorySlots.size(),
						true)) {
					return null;
				}
			} else if (!this.mergeItemStack(itemstack1, 0, this.tileEntity.getSizeInventory(), false)) {
				return null;
			}

			if (itemstack1.stackSize == 0) {
				slot.putStack((ItemStack) null);
			} else {
				slot.onSlotChanged();
			}
		}
		return itemstack;
	}

	@Override
	public boolean canInteractWith(EntityPlayer player) {
		return this.tileEntity.isUseableByPlayer(player);
	}

	public void onContainerClosed(EntityPlayer player) {
		super.onContainerClosed(player);
		this.tileEntity.closeInventory();
	}

}
