/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.recipes;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import re.alwyn974.novacraft.NovaCraft;

public class ArmorRecipes {

	private String[][] recipePatterns = new String[][] { { "XXX", "X X" }, { "X X", "XXX", "XXX" },{ "XXX", "X X", "X X" }, { "X X", "X X" } };
	private Item[][] recipeItems;

	public ArmorRecipes() {
		this.recipeItems = new Item[][] {
				{ NovaCraft.Insidium, NovaCraft.Spinelle, NovaCraft.Microlite, NovaCraft.Graphite },
				{ NovaCraft.InsidiumHelmet, NovaCraft.SpinelleHelmet, NovaCraft.MicroliteHelmet, NovaCraft.GraphiteHelmet },
				{ NovaCraft.InsidiumChestPlate, NovaCraft.SpinelleChestPlate, NovaCraft.MicroliteChestPlate, NovaCraft.GraphiteChestPlate },
				{ NovaCraft.InsidiumLeggings, NovaCraft.SpinelleLeggings, NovaCraft.MicroliteLeggings, NovaCraft.GraphiteLeggings },
				{ NovaCraft.InsidiumBoots, NovaCraft.SpinelleBoots, NovaCraft.MicroliteBoots, NovaCraft.GraphiteBoots } };
	}
	
	/**
	 *  Adds the Armor recipes.
	 */
	public void addRecipes() {
		for (int i = 0; i < this.recipeItems[0].length; i++) {
			Object material = this.recipeItems[0][i];

			for (int j = 0; j < this.recipeItems.length - 1; j++) {
				Item item = this.recipeItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { this.recipePatterns[j], 'X', material });
			}
		}
	}

}
