/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.recipes;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import re.alwyn974.novacraft.NovaCraft;

public class IngotsRecipes {

	private Object[][] recipeItems;

	public IngotsRecipes() {
		this.recipeItems = new Object[][] { { NovaCraft.InsidiumBlock, new ItemStack(NovaCraft.Insidium, 9) },
				{ NovaCraft.SpinelleBlock, new ItemStack(NovaCraft.Spinelle, 9) },
				{ NovaCraft.MicroliteBlock, new ItemStack(NovaCraft.Microlite, 9) },
				{ NovaCraft.GraphiteBlock, new ItemStack(NovaCraft.Graphite, 9) } };
	}

	/**
	 * Adds the ingot & block recipes.
	 */
	public void addRecipes() {
		for (int i = 0; i < this.recipeItems.length; ++i) {
			Block block = (Block) this.recipeItems[i][0];
			ItemStack itemstack = (ItemStack) this.recipeItems[i][1];
			GameRegistry.addRecipe(new ItemStack(block), new Object[] { "###", "###", "###", '#', itemstack });
			GameRegistry.addRecipe(itemstack, new Object[] { "#", '#', block });
		}
	}

}
