/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.recipes;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import re.alwyn974.novacraft.NovaCraft;

public class RodsRecipes {
	
	private String[][] recipePatterns = new String[][] { { " X ", "XYX", " X " }, { "  Y", " X ", "X  " }};
	private Item[][] recipeItemsDrops;
	private Item[][] recipeItemsRods;
	
	public RodsRecipes() {
		this.recipeItemsDrops = new Item[][] {
			{NovaCraft.Mure, NovaCraft.AppleHeal, NovaCraft.Banana, NovaCraft.Graphite},
			{NovaCraft.Mure, NovaCraft.AppleHeal, NovaCraft.Banana, NovaCraft.Graphite}
		};
		this.recipeItemsRods = new Item[][] {
			{NovaCraft.Mure, NovaCraft.AppleHeal, NovaCraft.Banana, NovaCraft.Graphite},
			{NovaCraft.RodOfView, NovaCraft.RodOfHeal, NovaCraft.RodOfFall, NovaCraft.RodOfHaste}
		};
	}
	
	/**
	 *  Adds the Rod recipes.
	 */
	public void addDropsRecipes() {
		for (int i = 0; i < this.recipeItemsDrops[0].length; i++) {
			Item material = this.recipeItemsDrops[0][i];

			for (int j = 0; j < this.recipeItemsDrops.length - 1; j++) {
				Item item = this.recipeItemsDrops[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item, 1, 1), new Object[] { this.recipePatterns[0], 'Y', material, 'X', NovaCraft.Spinelle });
			}
		}
	}
	
	/**
	 *  Adds the Rod recipes.
	 */
	public void addRodsRecipes() {	
		GameRegistry.addRecipe(new ItemStack(NovaCraft.Rod, 2), new Object[] { " X ", " Y ", " X ", 'X', new ItemStack(NovaCraft.SpinelleBlock), 'Y', new ItemStack(NovaCraft.IronStick) });
		for (int i = 0; i < this.recipeItemsRods[0].length; i++) {
			Item material = this.recipeItemsRods[0][i];

			for (int j = 0; j < this.recipeItemsRods.length - 1; j++) {
				Item item = this.recipeItemsRods[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { this.recipePatterns[1], 'Y', new ItemStack(material, 1, 1), 'X', NovaCraft.Rod });
			}
		}
	}
	
	
}	
