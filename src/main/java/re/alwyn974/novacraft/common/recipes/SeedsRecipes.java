/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.recipes;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import re.alwyn974.novacraft.NovaCraft;

public class SeedsRecipes {

	private String[][] recipePatterns = new String[][] { { "XXX", "X#X", "XXX" } };
	private Item[][] recipeItems;

	public SeedsRecipes() {
		this.recipeItems = new Item[][] { { NovaCraft.Graphite, NovaCraft.Microlite, NovaCraft.Spinelle },
				{ NovaCraft.GraphiteSeeds, NovaCraft.MicroliteSeeds, NovaCraft.SpinelleSeeds } };
	}

	/**
	 * Adds the Seeds recipes.
	 */
	public void addRecipes() {
		for (int i = 0; i < this.recipeItems[0].length; i++) {
			Item material = this.recipeItems[0][i];

			for (int j = 0; j < this.recipeItems.length - 1; j++) {
				Item item = this.recipeItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { this.recipePatterns[0], 'X', material, '#', Items.wheat_seeds });
			}
		}
	}

}
