/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.recipes;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import re.alwyn974.novacraft.NovaCraft;

public class ShardsRecipes {

	private Item[][] recipeItems;

	public ShardsRecipes() {
		this.recipeItems = new Item[][] {
				{ NovaCraft.GraphiteShard, NovaCraft.MicroliteShard, NovaCraft.SpinelleShard },
				{ NovaCraft.Graphite, NovaCraft.Microlite, NovaCraft.Spinelle } };
	}
	
	/**
	 *  Adds the Shard recipes.
	 */
	public void addRecipes() {
		for (int i = 0; i < this.recipeItems[0].length; i++) {
			Object material = this.recipeItems[0][i];

			for (int j = 0; j < this.recipeItems.length - 1; j++) {
				Item item = this.recipeItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { "###", "###", "###", '#', material });
			}
		}
	}
}
