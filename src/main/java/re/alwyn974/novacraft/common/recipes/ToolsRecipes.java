/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.recipes;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import re.alwyn974.novacraft.NovaCraft;

public class ToolsRecipes {

	private String[][] recipePatterns = new String[][] { { "XXX", " # ", " # " }, { "XX", "X#", " #" }, { "X", "#", "#" }, { "XX", " #", " #" } };
	private Item[][] recipeItems;

	public ToolsRecipes() {
		this.recipeItems = new Item[][] {
				{ NovaCraft.Insidium, NovaCraft.Spinelle, NovaCraft.Microlite, NovaCraft.Graphite },
				{ NovaCraft.InsidiumPickaxe, NovaCraft.SpinellePickaxe, NovaCraft.MicrolitePickaxe, NovaCraft.GraphitePickaxe },
				{ NovaCraft.InsidiumAxe, NovaCraft.SpinelleAxe, NovaCraft.MicroliteAxe, NovaCraft.GraphiteAxe },
				{ NovaCraft.InsidiumShovel, NovaCraft.SpinelleShovel, NovaCraft.MicroliteShovel, NovaCraft.GraphiteShovel },
				{ NovaCraft.InsidiumHoe, NovaCraft.SpinelleHoe, NovaCraft.MicroliteHoe, NovaCraft.GraphiteHoe } };
	}

	/**
	 * Adds the tool recipes.
	 */
	public void addRecipes() {
		for (int i = 0; i < this.recipeItems[0].length; ++i) {
			Object material = this.recipeItems[0][i];

			for (int j = 0; j < this.recipeItems.length - 1; ++j) {
				Item item = this.recipeItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { this.recipePatterns[j], '#', Items.stick, 'X', material });
			}
		}
	}
}
