/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.recipes;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import re.alwyn974.novacraft.NovaCraft;

public class WeaponsRecipes {

	private String[][] recipeSwordsPattern = new String[][] { { "X", "X", "#" } };
	private String[][] recipeBattlePatterns = new String[][] { { "XXX", "X# ", " # " }, { "XXX", "X#X", " # " } };
	private String[][] recipeBowsPattern = new String[][] { { " #X", "# X", " #X" } };

	private Item[][] recipeSwordsItems, recipeBattleItems, recipeBowsItems;

	public WeaponsRecipes() {
		this.recipeSwordsItems = new Item[][] {
				{ NovaCraft.Insidium, NovaCraft.Spinelle, NovaCraft.Microlite, NovaCraft.Graphite },
				{ NovaCraft.InsidiumSword, NovaCraft.SpinelleSword, NovaCraft.MicroliteSword, NovaCraft.GraphiteSword } };

		this.recipeBattleItems = new Item[][] {
				{ NovaCraft.Insidium, NovaCraft.Spinelle, NovaCraft.Microlite, NovaCraft.Graphite },
				{ NovaCraft.InsidiumBattleAxe, NovaCraft.SpinelleBattleAxe, NovaCraft.MicroliteBattleAxe,
						NovaCraft.GraphiteBattleAxe },
				{ NovaCraft.InsidiumBattleHammer, NovaCraft.SpinelleBattleHammer, NovaCraft.MicroliteBattleHammer, NovaCraft.GraphiteBattleHammer } };

		this.recipeBowsItems = new Item[][] {
				{ NovaCraft.Insidium, NovaCraft.Spinelle, NovaCraft.Microlite, NovaCraft.Graphite, Items.diamond },
				{ NovaCraft.InsidiumBow, NovaCraft.SpinelleBow, NovaCraft.MicroliteBow, NovaCraft.GraphiteBow, NovaCraft.DiamondBow } };
	}

	/**
	 * Adds the Sword recipes.
	 */
	public void addSwordRecipes() {
		for (int i = 0; i < this.recipeSwordsItems[0].length; i++) {
			Object material = this.recipeSwordsItems[0][i];

			for (int j = 0; j < this.recipeSwordsItems.length - 1; ++j) {
				Item item = this.recipeSwordsItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { this.recipeSwordsPattern[j], '#', Items.stick, 'X', material });
			}
		}
	}

	/**
	 * Adds the BattleAxe & BattleHammer recipes.
	 */
	public void addBattleRecipes() {
		for (int i = 0; i < this.recipeBattleItems[0].length; i++) {
			Object material = this.recipeBattleItems[0][i];

			for (int j = 0; j < this.recipeBattleItems.length - 1; j++) {
				Item item = this.recipeBattleItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { this.recipeBattlePatterns[j], '#', NovaCraft.IronStick, 'X', material });
			}
		}
	}

	/**
	 * Adds the Bow recipes.
	 */
	public void addBowRecipes() {
		for (int i = 0; i < this.recipeBowsItems[0].length; i++) {
			Object material = this.recipeBowsItems[0][i];
			
			for (int j = 0; j < this.recipeBowsItems.length - 1; j++) {
				Item item = this.recipeBowsItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { this.recipeBowsPattern[j], 'X', Items.string, '#', material });
			}
		}
	}

}
