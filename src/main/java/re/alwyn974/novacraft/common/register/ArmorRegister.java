/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.register;

import cpw.mods.fml.common.registry.GameRegistry;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.item.NItemArmor;

public class ArmorRegister {
	
	public ArmorRegister() {
		insidiumArmorRegister();
		spinelleArmorRegister();
		microliteArmorRegister();
		graphiteArmorRegister();
	}
	
	public void insidiumArmorRegister() {
		NovaCraft.InsidiumHelmet = new NItemArmor(NovaCraft.InsidiumArmor, 0, "insidium_helmet", NovaCraft.Insidium).setMaxDamage(1350);
		NovaCraft.InsidiumChestPlate = new NItemArmor(NovaCraft.InsidiumArmor, 1, "insidium_chestplate", NovaCraft.Insidium).setMaxDamage(1560);
		NovaCraft.InsidiumLeggings = new NItemArmor(NovaCraft.InsidiumArmor, 2, "insidium_leggings", NovaCraft.Insidium).setMaxDamage(1410);
		NovaCraft.InsidiumBoots = new NItemArmor(NovaCraft.InsidiumArmor, 3, "insidium_boots", NovaCraft.Insidium).setMaxDamage(1350);
				
		GameRegistry.registerItem(NovaCraft.InsidiumHelmet, "insidium_helmet");
		GameRegistry.registerItem(NovaCraft.InsidiumChestPlate, "insidium_chestplate");
		GameRegistry.registerItem(NovaCraft.InsidiumLeggings, "insidium_leggings");
		GameRegistry.registerItem(NovaCraft.InsidiumBoots, "insidium_boots");
	}
	
	public void spinelleArmorRegister() {
		NovaCraft.SpinelleHelmet = new NItemArmor(NovaCraft.SpinelleArmor, 0, "spinelle_helmet", NovaCraft.Spinelle).setMaxDamage(830);
		NovaCraft.SpinelleChestPlate = new NItemArmor(NovaCraft.SpinelleArmor, 1, "spinelle_chestplate", NovaCraft.Spinelle).setMaxDamage(1160);
		NovaCraft.SpinelleLeggings = new NItemArmor(NovaCraft.SpinelleArmor, 2, "spinelle_leggings", NovaCraft.Spinelle).setMaxDamage(1160);
		NovaCraft.SpinelleBoots = new NItemArmor(NovaCraft.SpinelleArmor, 3, "spinelle_boots", NovaCraft.Spinelle).setMaxDamage(890);
		
		GameRegistry.registerItem(NovaCraft.SpinelleHelmet, "spinelle_helmet");
		GameRegistry.registerItem(NovaCraft.SpinelleChestPlate, "spinelle_chestplate");
		GameRegistry.registerItem(NovaCraft.SpinelleLeggings, "spinelle_leggings");
		GameRegistry.registerItem(NovaCraft.SpinelleBoots, "spinelle_boots");
	}
	
	public void microliteArmorRegister() {
		NovaCraft.MicroliteHelmet = new NItemArmor(NovaCraft.MicroliteArmor, 0, "microlite_helmet", NovaCraft.Microlite).setMaxDamage(980);
		NovaCraft.MicroliteChestPlate = new NItemArmor(NovaCraft.MicroliteArmor, 1, "microlite_chestplate", NovaCraft.Microlite).setMaxDamage(1140);
		NovaCraft.MicroliteLeggings = new NItemArmor(NovaCraft.MicroliteArmor, 2, "microlite_leggings", NovaCraft.Microlite).setMaxDamage(1060);
		NovaCraft.MicroliteBoots = new NItemArmor(NovaCraft.MicroliteArmor, 3, "microlite_boots", NovaCraft.Microlite).setMaxDamage(980);
		
		GameRegistry.registerItem(NovaCraft.MicroliteHelmet, "microlite_helmet");
		GameRegistry.registerItem(NovaCraft.MicroliteChestPlate, "microlite_chestplate");
		GameRegistry.registerItem(NovaCraft.MicroliteLeggings, "microlite_leggings");
		GameRegistry.registerItem(NovaCraft.MicroliteBoots, "microlite_boots");
	}
	
	public void graphiteArmorRegister() {
		NovaCraft.GraphiteHelmet = new NItemArmor(NovaCraft.GraphiteArmor, 0, "graphite_helmet", NovaCraft.Graphite).setMaxDamage(710);
		NovaCraft.GraphiteChestPlate = new NItemArmor(NovaCraft.GraphiteArmor, 1, "graphite_chestplate", NovaCraft.Graphite).setMaxDamage(920);
		NovaCraft.GraphiteLeggings = new NItemArmor(NovaCraft.GraphiteArmor, 2, "graphite_leggings", NovaCraft.Graphite).setMaxDamage(810);
		NovaCraft.GraphiteBoots = new NItemArmor(NovaCraft.GraphiteArmor, 3, "graphite_boots", NovaCraft.Graphite).setMaxDamage(770);
		
		GameRegistry.registerItem(NovaCraft.GraphiteHelmet, "graphite_helmet");
		GameRegistry.registerItem(NovaCraft.GraphiteChestPlate, "graphite_chestplate");
		GameRegistry.registerItem(NovaCraft.GraphiteLeggings, "graphite_leggings");
		GameRegistry.registerItem(NovaCraft.GraphiteBoots, "graphite_boots");
	}
}
