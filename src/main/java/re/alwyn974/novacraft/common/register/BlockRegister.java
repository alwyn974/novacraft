/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.register;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.BlockFence;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.block.BananaBlockLeaves;
import re.alwyn974.novacraft.block.BananaBlockSapling;
import re.alwyn974.novacraft.block.BlockGraphiteChest;
import re.alwyn974.novacraft.block.BlockGraphiteLadder;
import re.alwyn974.novacraft.block.BlockInvisible;
import re.alwyn974.novacraft.block.BlockObsidianTrapdoor;
import re.alwyn974.novacraft.block.BlockReinforcedObsidian;
import re.alwyn974.novacraft.block.NBlock;
import re.alwyn974.novacraft.block.NBlockOre;
import re.alwyn974.novacraft.block.NBlockSlab;
import re.alwyn974.novacraft.block.NBlockStairs;
import re.alwyn974.novacraft.block.crops.BlockAppleHealCrops;
import re.alwyn974.novacraft.block.crops.BlockBananaCrops;
import re.alwyn974.novacraft.block.crops.BlockGraphiteCrops;
import re.alwyn974.novacraft.block.crops.BlockMicroliteCrops;
import re.alwyn974.novacraft.block.crops.BlockMureCrops;
import re.alwyn974.novacraft.block.crops.BlockSpinelleCrops;
import re.alwyn974.novacraft.item.NItemBlock;
import re.alwyn974.novacraft.item.NItemBlockSlab;

public class BlockRegister {

	public BlockRegister() {
		oreRegister();
		blockRegister();
		bananaTreeBlockRegister();
		blockCropRegister();
		blockDecoRegister();
	}

	public void oreRegister() {
		NovaCraft.InsidiumOre = new NBlockOre("insidium_ore");
		NovaCraft.SpinelleOre = new NBlockOre("spinelle_ore");
		NovaCraft.MicroliteOre = new NBlockOre("microlite_ore");
		NovaCraft.GraphiteOre = new NBlockOre("graphite_ore");
		NovaCraft.RandomOre = new NBlockOre("random_ore");

		GameRegistry.registerBlock(NovaCraft.InsidiumOre, NItemBlock.class, "insidium_ore");
		GameRegistry.registerBlock(NovaCraft.SpinelleOre, NItemBlock.class, "spinelle_ore");
		GameRegistry.registerBlock(NovaCraft.MicroliteOre, NItemBlock.class, "microlite_ore");
		GameRegistry.registerBlock(NovaCraft.GraphiteOre, NItemBlock.class, "graphite_ore");
		GameRegistry.registerBlock(NovaCraft.RandomOre, "random_ore");

		OreDictionary.registerOre("insidium_ore", NovaCraft.InsidiumOre);
		OreDictionary.registerOre("spinelle_ore", NovaCraft.SpinelleOre);
		OreDictionary.registerOre("microlite_ore", NovaCraft.MicroliteOre);
		OreDictionary.registerOre("graphite_ore", NovaCraft.GraphiteOre);
		OreDictionary.registerOre("random_ore", NovaCraft.RandomOre);
	}

	public void blockRegister() {
		NovaCraft.InsidiumBlock = new NBlock(Material.rock, "insidium_block");
		NovaCraft.SpinelleBlock = new NBlock(Material.rock, "spinelle_block");
		NovaCraft.MicroliteBlock = new NBlock(Material.rock, "microlite_block");
		NovaCraft.GraphiteBlock = new NBlock(Material.rock, "graphite_block");
		NovaCraft.CaveBlock = new NBlock(Material.rock, "cave_block");
		NovaCraft.ReinforcedObsidian = new BlockReinforcedObsidian("reinforced_obsidian");
		NovaCraft.GraphiteChest = new BlockGraphiteChest("graphite_chest");
		NovaCraft.InvisibleBlock = new BlockInvisible();
		
		GameRegistry.registerBlock(NovaCraft.InsidiumBlock, NItemBlock.class, "insidium_block");
		GameRegistry.registerBlock(NovaCraft.SpinelleBlock, NItemBlock.class, "spinelle_block");
		GameRegistry.registerBlock(NovaCraft.MicroliteBlock, NItemBlock.class, "microlite_block");
		GameRegistry.registerBlock(NovaCraft.GraphiteBlock, NItemBlock.class, "graphite_block");
		GameRegistry.registerBlock(NovaCraft.CaveBlock, "cave_block");
		GameRegistry.registerBlock(NovaCraft.ReinforcedObsidian, NItemBlock.class, "reinforced_obsidian");
		GameRegistry.registerBlock(NovaCraft.GraphiteChest, NItemBlock.class, "graphite_chest");
		GameRegistry.registerBlock(NovaCraft.InvisibleBlock, "invisible_block");
	}
	
	public void bananaTreeBlockRegister() {
		NovaCraft.BananaLeaves = new BananaBlockLeaves("banana_leaves");
		NovaCraft.BananaSapling = new BananaBlockSapling("banana_sapling");
		
		GameRegistry.registerBlock(NovaCraft.BananaLeaves, "banana_leaves");
		GameRegistry.registerBlock(NovaCraft.BananaSapling, "banana_sapling");
	}

	public void blockCropRegister() {
		NovaCraft.AppleHealCrops = new BlockAppleHealCrops();
		NovaCraft.BananaCrops = new BlockBananaCrops();
		NovaCraft.MureCrops = new BlockMureCrops();
		NovaCraft.SpinelleCrops = new BlockSpinelleCrops();
		NovaCraft.MicroliteCrops = new BlockMicroliteCrops();
		NovaCraft.GraphiteCrops = new BlockGraphiteCrops();

		GameRegistry.registerBlock(NovaCraft.AppleHealCrops, "appleheal_crops");
		GameRegistry.registerBlock(NovaCraft.BananaCrops, "banana_crops");
		GameRegistry.registerBlock(NovaCraft.MureCrops, "mure_crops");
		GameRegistry.registerBlock(NovaCraft.SpinelleCrops, NItemBlock.class, "spinelle_crops");
		GameRegistry.registerBlock(NovaCraft.MicroliteCrops, NItemBlock.class, "microlite_crops");
		GameRegistry.registerBlock(NovaCraft.GraphiteCrops, NItemBlock.class, "graphite_crops");
	}

	public void blockDecoRegister() {
		for (int i = 0; i < 16; ++i) {
			NovaCraft.HardenedClayStairs[i] = new NBlockStairs(Blocks.stained_hardened_clay, "stained_hardened_clay_stairs_" + ItemDye.field_150921_b[~i & 15], i);

			String slabName = "block_hardened_clay_slab_" + ItemDye.field_150921_b[~i & 15];
			String doubleSlabName = "double_hardened_clay_slab_" + ItemDye.field_150921_b[~i & 15];
			String textureName = "hardened_clay_stained_" + ItemDye.field_150921_b[~i & 15];
			
			NovaCraft.NovacraftSlabs[i] = new NBlockSlab(slabName, false, Blocks.stained_hardened_clay).setBlockTextureName(textureName);
			NovaCraft.NovacraftDoubleSlabs[i] = new NBlockSlab(doubleSlabName, true, Blocks.stained_hardened_clay).setBlockTextureName(textureName);

			GameRegistry.registerBlock(NovaCraft.NovacraftSlabs[i], NItemBlockSlab.class, slabName, new Object[] { NovaCraft.NovacraftSlabs[i], NovaCraft.NovacraftDoubleSlabs[i], false });
			GameRegistry.registerBlock(NovaCraft.NovacraftDoubleSlabs[i], null, doubleSlabName, new Object[] { NovaCraft.NovacraftSlabs[i], NovaCraft.NovacraftDoubleSlabs[i], true });

			((NBlockSlab) NovaCraft.NovacraftDoubleSlabs[i]).setSingleSlab(NovaCraft.NovacraftSlabs[i]);
			
			GameRegistry.addRecipe(new ItemStack(NovaCraft.NovacraftSlabs[i], 6, 0), new Object[] { "###", '#', new ItemStack(Blocks.stained_hardened_clay, 1, i) });
		}
		NovaCraft.HardenedClayStairs[16] = new NBlockStairs(Blocks.hardened_clay, "hardened_clay_stairs", 0);
		NovaCraft.ObsidianStairs = new NBlockStairs(Blocks.obsidian, "obsidian_stairs", 0);
		NovaCraft.GlowStoneStairs = new NBlockStairs(Blocks.glowstone, "glowstone_stairs", 0).setLightLevel(1.0F).setLightOpacity(0);
		NovaCraft.StoneStairs = new NBlockStairs(Blocks.stone, "stone_stairs", 0);

		NovaCraft.NovacraftSlabs[16] = new NBlockSlab("block_hardened_clay_slab", false, Blocks.hardened_clay).setBlockTextureName("hardened_clay");
		NovaCraft.NovacraftDoubleSlabs[16] = new NBlockSlab("double_hardened_clay_slab", true, Blocks.hardened_clay).setBlockTextureName("hardened_clay");
		GameRegistry.registerBlock(NovaCraft.NovacraftSlabs[16], NItemBlockSlab.class, "block_hardened_clay_slab", new Object[] { NovaCraft.NovacraftSlabs[16], NovaCraft.NovacraftDoubleSlabs[16], false });
		GameRegistry.registerBlock(NovaCraft.NovacraftDoubleSlabs[16], null, "double_hardened_clay_slab", new Object[] { NovaCraft.NovacraftSlabs[16], NovaCraft.NovacraftDoubleSlabs[16], true });
		((NBlockSlab) NovaCraft.NovacraftDoubleSlabs[16]).setSingleSlab(NovaCraft.NovacraftSlabs[16]);
		
		NovaCraft.ObsidianSlab = new NBlockSlab("obsidian_slab", false, Blocks.obsidian).setBlockTextureName("obsidian").setHardness(50.0F).setResistance(2000.0F);
		NovaCraft.ObsidianDoubleSlab = new NBlockSlab("obsidian_double_slab", true, Blocks.obsidian).setBlockTextureName("obsidian").setHardness(50.0F).setResistance(2000.0F);
		GameRegistry.registerBlock(NovaCraft.ObsidianSlab, NItemBlockSlab.class, "obsidian_slab", new Object[] { NovaCraft.ObsidianSlab, NovaCraft.ObsidianDoubleSlab, false });
		GameRegistry.registerBlock(NovaCraft.ObsidianDoubleSlab, null, "obsidian_double_slab", new Object[] { NovaCraft.ObsidianSlab, NovaCraft.ObsidianDoubleSlab, true });
		((NBlockSlab) NovaCraft.ObsidianDoubleSlab).setSingleSlab(NovaCraft.ObsidianSlab);

		NovaCraft.GlowStoneSlab = new NBlockSlab("glowstone_slab", false, Blocks.glowstone).setBlockTextureName("glowstone").setLightLevel(1.0F).setLightOpacity(0);
		NovaCraft.GlowStoneDoubleSlab = new NBlockSlab("glowstone_double_slab", true, Blocks.glowstone).setBlockTextureName("glowstone").setLightLevel(1.0F).setLightOpacity(0);
		GameRegistry.registerBlock(NovaCraft.GlowStoneSlab, NItemBlockSlab.class, "glowstone_slab",new Object[] { NovaCraft.GlowStoneSlab, NovaCraft.GlowStoneDoubleSlab, false });
		GameRegistry.registerBlock(NovaCraft.GlowStoneDoubleSlab, null, "glowstone_double_slab",new Object[] { NovaCraft.GlowStoneSlab, NovaCraft.GlowStoneDoubleSlab, true });
		((NBlockSlab) NovaCraft.GlowStoneDoubleSlab).setSingleSlab(NovaCraft.GlowStoneSlab);

		NovaCraft.ObsidianFence = new BlockFence("obsidian", Material.rock).setCreativeTab(NovaCraft.NovaCraftCreativeTabs).setBlockName("obsidian_fence").setHardness(50.0F).setResistance(2000.0F);
		NovaCraft.GlowStoneFence = new BlockFence("glowstone", Material.glass).setCreativeTab(NovaCraft.NovaCraftCreativeTabs).setBlockName("glowstone_fence").setLightLevel(1.0F).setLightOpacity(0);
		NovaCraft.IronFence = new BlockFence("iron_block", Material.iron).setCreativeTab(NovaCraft.NovaCraftCreativeTabs).setBlockName("iron_fence");
		
		NovaCraft.ObsidianTrapdoor = new BlockObsidianTrapdoor("obsidian_trapdoor").setHardness(16.0F);
		NovaCraft.GraphiteLadder = new BlockGraphiteLadder("graphite_ladder");
		
		GameRegistry.registerBlock(NovaCraft.ObsidianFence, "obsidian_fence");
		GameRegistry.registerBlock(NovaCraft.GlowStoneFence, "glowstone_fence");
		GameRegistry.registerBlock(NovaCraft.IronFence, "iron_fence");	
		GameRegistry.registerBlock(NovaCraft.ObsidianTrapdoor, "obsidian_trapdoor");
		GameRegistry.registerBlock(NovaCraft.GraphiteLadder, "graphite_ladder");
	}

}
