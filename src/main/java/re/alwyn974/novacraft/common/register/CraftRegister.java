/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.register;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.common.recipes.ArmorRecipes;
import re.alwyn974.novacraft.common.recipes.IngotsRecipes;
import re.alwyn974.novacraft.common.recipes.RodsRecipes;
import re.alwyn974.novacraft.common.recipes.SeedsRecipes;
import re.alwyn974.novacraft.common.recipes.ShardsRecipes;
import re.alwyn974.novacraft.common.recipes.ToolsRecipes;
import re.alwyn974.novacraft.common.recipes.WeaponsRecipes;

public class CraftRegister {

	public CraftRegister() {
		new ArmorRecipes().addRecipes();
		new IngotsRecipes().addRecipes();
		itemsRecipes();
		new RodsRecipes().addDropsRecipes();
		new RodsRecipes().addRodsRecipes();
		smeltingCraftRegister();
		new SeedsRecipes().addRecipes();
		new ShardsRecipes().addRecipes();
		new ToolsRecipes().addRecipes();
		new WeaponsRecipes().addBattleRecipes();
		new WeaponsRecipes().addBowRecipes();
		new WeaponsRecipes().addSwordRecipes();
	}

	public void itemsRecipes() {
		GameRegistry.addRecipe(new ItemStack(NovaCraft.IronStick, 1), new Object[] { " X ", " X ", 'X', new ItemStack(Blocks.iron_block) });
		GameRegistry.addRecipe(new ItemStack(NovaCraft.Petard, 1), new Object[] { " X ", "YZY", " X ", 'X', Blocks.tnt, 'Y', Items.gunpowder, 'Z', new ItemStack(NovaCraft.Insidium) });
		GameRegistry.addRecipe(new ItemStack(NovaCraft.ReinforcedObsidian), new Object[] { "XXX", "YYY", "XXX", 'X', Blocks.iron_bars, 'Y', Blocks.obsidian });	
		GameRegistry.addRecipe(new ItemStack(NovaCraft.NovacraftSlabs[16], 6, 0), new Object[] { "###", '#', Blocks.hardened_clay});
		GameRegistry.addRecipe(new ItemStack(NovaCraft.ObsidianSlab, 6, 0), new Object[] { "###", '#', Blocks.obsidian });
		GameRegistry.addRecipe(new ItemStack(NovaCraft.GlowStoneSlab, 6, 0), new Object[] { "###", '#', Blocks.glowstone });
		GameRegistry.addRecipe(new ItemStack(NovaCraft.GraphiteLadder, 6), new Object[] {"# #", "###", "# #", '#', NovaCraft.Graphite});
		GameRegistry.addRecipe(new ItemStack(NovaCraft.IronFence, 2), new Object[] {"###", "###", '#', Items.iron_ingot});
		GameRegistry.addRecipe(new ItemStack(NovaCraft.GlowStoneFence, 2), new Object[] {"###", "###", '#', Blocks.glowstone});
		GameRegistry.addRecipe(new ItemStack(NovaCraft.ObsidianFence, 2), new Object[] {"###", "###", '#', Blocks.obsidian});
		GameRegistry.addRecipe(new ItemStack(NovaCraft.ObsidianTrapdoor, 2), new Object[] {"###", "###", '#', Blocks.obsidian});
	}

	public void smeltingCraftRegister() {
		GameRegistry.addSmelting(NovaCraft.InsidiumOre, new ItemStack(NovaCraft.Insidium), 5.0F);
		GameRegistry.addSmelting(NovaCraft.SpinelleOre, new ItemStack(NovaCraft.Spinelle), 4.0F);
		GameRegistry.addSmelting(NovaCraft.MicroliteOre, new ItemStack(NovaCraft.Microlite), 3.0F);
		GameRegistry.addSmelting(NovaCraft.GraphiteOre, new ItemStack(NovaCraft.Graphite), 2.0F);
	}

}
