/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.register;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.item.ItemKeys;
import re.alwyn974.novacraft.item.ItemPetard;
import re.alwyn974.novacraft.item.NItem;
import re.alwyn974.novacraft.item.NItemFarm;
import re.alwyn974.novacraft.item.NItemSeeds;
import re.alwyn974.novacraft.item.RodOfFall;
import re.alwyn974.novacraft.item.RodOfHaste;
import re.alwyn974.novacraft.item.RodOfHeal;
import re.alwyn974.novacraft.item.RodOfView;

public class ItemRegister {
	
	public ItemRegister() {
		itemRegister();
		seedRegister();
		dropRegister();
		rodRegister();
	}
	
	public void itemRegister() {
		NovaCraft.Microlite = new NItem("microlite");
		NovaCraft.Spinelle = new NItem("spinelle");
		NovaCraft.Insidium = new NItem("insidium");
		NovaCraft.Graphite = new NItem("graphite");
		NovaCraft.IronStick = new NItem("iron_stick");
		NovaCraft.Petard = new ItemPetard("petard");
		
		String[] textures = { "daily_key", "diamond_key", "emerald_key", "event_key", "gold_key", "vote_key" };
		for (int i=0; i < textures.length; i++) {
			NovaCraft.Keys[i] = new ItemKeys(textures[i]);
		}
		
		GameRegistry.registerItem(NovaCraft.Microlite, "microlite");
		GameRegistry.registerItem(NovaCraft.Spinelle, "spinelle");
		GameRegistry.registerItem(NovaCraft.Insidium, "insidium");
		GameRegistry.registerItem(NovaCraft.Graphite, "graphite");
		GameRegistry.registerItem(NovaCraft.IronStick, "iron_stick");
		GameRegistry.registerItem(NovaCraft.Petard, "petard");
	}
	
	public void seedRegister() {
		NovaCraft.AppleHealSeeds = new NItemSeeds(NovaCraft.AppleHealCrops, Blocks.farmland, "appleheal_seeds");
		NovaCraft.BananaSeeds = new NItemSeeds(NovaCraft.BananaCrops, Blocks.farmland, "banana_seeds");
		NovaCraft.MureSeeds = new NItemSeeds(NovaCraft.MureCrops, Blocks.farmland, "mure_seeds");
		NovaCraft.GraphiteSeeds = new NItemSeeds(NovaCraft.GraphiteCrops, Blocks.farmland, "graphite_seeds");
		NovaCraft.SpinelleSeeds = new NItemSeeds(NovaCraft.SpinelleCrops, Blocks.farmland, "spinelle_seeds");
		NovaCraft.MicroliteSeeds = new NItemSeeds(NovaCraft.MicroliteCrops, Blocks.farmland, "microlite_seeds");
		
		GameRegistry.registerItem(NovaCraft.AppleHealSeeds, "appleheal_seeds");
		GameRegistry.registerItem(NovaCraft.BananaSeeds, "banana_seeds");
		GameRegistry.registerItem(NovaCraft.MureSeeds, "mure_seeds");
		GameRegistry.registerItem(NovaCraft.GraphiteSeeds, "graphite_seeds");
		GameRegistry.registerItem(NovaCraft.SpinelleSeeds, "spinelle_seeds");
		GameRegistry.registerItem(NovaCraft.MicroliteSeeds, "microlite_seeds");
	}
	
	public void dropRegister() {
		NovaCraft.AppleHeal = new NItemFarm("appleheal", 4, 2, false);
		NovaCraft.Banana = new NItemFarm("banana", 4, 2, false);
		NovaCraft.Mure = new NItemFarm("mure", 4, 2, false);
		NovaCraft.GraphiteShard = new NItem("graphite_shard");
		NovaCraft.MicroliteShard = new NItem("microlite_shard");
		NovaCraft.SpinelleShard = new NItem("spinelle_shard");
				
		GameRegistry.registerItem(NovaCraft.AppleHeal, "appleheal");
		GameRegistry.registerItem(NovaCraft.Banana, "banana");
		GameRegistry.registerItem(NovaCraft.Mure, "mure");
		GameRegistry.registerItem(NovaCraft.GraphiteShard, "graphite_shard");
		GameRegistry.registerItem(NovaCraft.MicroliteShard, "microlite_shard");
		GameRegistry.registerItem(NovaCraft.SpinelleShard, "spinelle_shard");
	}
	
	public void rodRegister() {
		NovaCraft.Rod = new NItem("rod");
		NovaCraft.RodOfHeal = new RodOfHeal("rod_of_heal");
		NovaCraft.RodOfFall = new RodOfFall("rod_of_fall");
		NovaCraft.RodOfView = new RodOfView("rod_of_view");
		NovaCraft.RodOfHaste = new RodOfHaste("rod_of_haste");
			
		GameRegistry.registerItem(NovaCraft.Rod, "rod");
		GameRegistry.registerItem(NovaCraft.RodOfHeal, "rod_of_heal");
		GameRegistry.registerItem(NovaCraft.RodOfFall, "rod_of_fall");
		GameRegistry.registerItem(NovaCraft.RodOfView, "rod_of_view");
		GameRegistry.registerItem(NovaCraft.RodOfHaste, "rod_of_haste");
	}

}
