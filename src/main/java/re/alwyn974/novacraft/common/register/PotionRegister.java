/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.register;

import java.awt.Color;

import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.potion.SlowFallEffect;
import re.alwyn974.novacraft.potion.ViewEffect;

public class PotionRegister {
	
	public PotionRegister() {
		NovaCraft.SlowFall = new SlowFallEffect(30, false, Color.white.getRGB(), "slowfall");
		NovaCraft.ViewEffect = new ViewEffect(31, false, Color.white.getRGB(), "view_effect");
	}

}
