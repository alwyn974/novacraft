/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.register;

import net.minecraft.item.ItemStack;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraftforge.common.ChestGenHooks;
import re.alwyn974.novacraft.NovaCraft;

public class SeedGenRegister {
	
	public SeedGenRegister() {
		appleHealSeedGenRegister();
		bananaSeedGenRegister();
		mureSeedGenRegister();
	}
	
	public void appleHealSeedGenRegister() {
		WeightedRandomChestContent wrcc = new WeightedRandomChestContent(new ItemStack(NovaCraft.AppleHealSeeds), 2, 7, 3);
		ChestGenHooks.getInfo(ChestGenHooks.DUNGEON_CHEST).addItem(wrcc);
		ChestGenHooks.getInfo(ChestGenHooks.MINESHAFT_CORRIDOR).addItem(wrcc);
		ChestGenHooks.getInfo(ChestGenHooks.PYRAMID_DESERT_CHEST).addItem(wrcc);
		ChestGenHooks.getInfo(ChestGenHooks.PYRAMID_JUNGLE_CHEST).addItem(wrcc);
		ChestGenHooks.getInfo(ChestGenHooks.VILLAGE_BLACKSMITH).addItem(wrcc);
	}
	
	public void bananaSeedGenRegister() {
		WeightedRandomChestContent wrcc = new WeightedRandomChestContent(new ItemStack(NovaCraft.BananaSeeds), 2, 7, 3);
		ChestGenHooks.getInfo(ChestGenHooks.DUNGEON_CHEST).addItem(wrcc);
		ChestGenHooks.getInfo(ChestGenHooks.MINESHAFT_CORRIDOR).addItem(wrcc);
		ChestGenHooks.getInfo(ChestGenHooks.PYRAMID_DESERT_CHEST).addItem(wrcc);
		ChestGenHooks.getInfo(ChestGenHooks.PYRAMID_JUNGLE_CHEST).addItem(wrcc);
		ChestGenHooks.getInfo(ChestGenHooks.VILLAGE_BLACKSMITH).addItem(wrcc);
	}
	
	public void mureSeedGenRegister() {
		WeightedRandomChestContent wrcc = new WeightedRandomChestContent(new ItemStack(NovaCraft.MureSeeds), 2, 7, 3);
		ChestGenHooks.getInfo(ChestGenHooks.DUNGEON_CHEST).addItem(wrcc);
		ChestGenHooks.getInfo(ChestGenHooks.MINESHAFT_CORRIDOR).addItem(wrcc);
		ChestGenHooks.getInfo(ChestGenHooks.PYRAMID_DESERT_CHEST).addItem(wrcc);
		ChestGenHooks.getInfo(ChestGenHooks.PYRAMID_JUNGLE_CHEST).addItem(wrcc);
		ChestGenHooks.getInfo(ChestGenHooks.VILLAGE_BLACKSMITH).addItem(wrcc);
	}

}
