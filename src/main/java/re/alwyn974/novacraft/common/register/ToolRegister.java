/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.register;

import cpw.mods.fml.common.registry.GameRegistry;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.item.tools.NItemAxe;
import re.alwyn974.novacraft.item.tools.NItemHoe;
import re.alwyn974.novacraft.item.tools.NItemPickaxe;
import re.alwyn974.novacraft.item.tools.NItemShovel;

public class ToolRegister {
	
	public ToolRegister() {
		insidiumToolRegister();
		spinelleToolRegister();
		microliteToolRegister();
		graphiteToolRegister();
	}
	
	public void insidiumToolRegister() {
		NovaCraft.InsidiumPickaxe = new NItemPickaxe(NovaCraft.InsidiumTool, "insidium_pickaxe");
		NovaCraft.InsidiumAxe = new NItemAxe(NovaCraft.InsidiumTool, "insidium_axe");
		NovaCraft.InsidiumShovel = new NItemShovel(NovaCraft.InsidiumTool, "insidium_shovel");
		NovaCraft.InsidiumHoe = new NItemHoe(NovaCraft.InsidiumTool, "insidium_hoe");
		
		GameRegistry.registerItem(NovaCraft.InsidiumPickaxe, "insidium_pickaxe");
		GameRegistry.registerItem(NovaCraft.InsidiumAxe, "insidium_axe");
		GameRegistry.registerItem(NovaCraft.InsidiumShovel, "insidium_shovel");
		GameRegistry.registerItem(NovaCraft.InsidiumHoe, "insidium_hoe");
	}
	
	public void spinelleToolRegister() {
		NovaCraft.SpinellePickaxe = new NItemPickaxe(NovaCraft.SpinelleTool, "spinelle_pickaxe");
		NovaCraft.SpinelleAxe = new NItemAxe(NovaCraft.SpinelleTool, "spinelle_axe");
		NovaCraft.SpinelleShovel = new NItemShovel(NovaCraft.SpinelleTool, "spinelle_shovel");
		NovaCraft.SpinelleHoe = new NItemHoe(NovaCraft.SpinelleTool, "spinelle_hoe");
		
		GameRegistry.registerItem(NovaCraft.SpinellePickaxe, "spinelle_pickaxe");
		GameRegistry.registerItem(NovaCraft.SpinelleAxe, "spinelle_axe");
		GameRegistry.registerItem(NovaCraft.SpinelleShovel, "spinelle_shovel");
		GameRegistry.registerItem(NovaCraft.SpinelleHoe, "spinelle_hoe");
	}
	
	public void microliteToolRegister() {
		NovaCraft.MicrolitePickaxe = new NItemPickaxe(NovaCraft.MicroliteTool, "microlite_pickaxe");
		NovaCraft.MicroliteAxe = new NItemAxe(NovaCraft.MicroliteTool, "microlite_axe");
		NovaCraft.MicroliteShovel = new NItemShovel(NovaCraft.MicroliteTool, "microlite_shovel");
		NovaCraft.MicroliteHoe = new NItemHoe(NovaCraft.MicroliteTool, "microlite_hoe");
		
		GameRegistry.registerItem(NovaCraft.MicrolitePickaxe, "microlite_pickaxe");
		GameRegistry.registerItem(NovaCraft.MicroliteAxe, "microlite_axe");
		GameRegistry.registerItem(NovaCraft.MicroliteShovel, "microlite_shovel");
		GameRegistry.registerItem(NovaCraft.MicroliteHoe, "microlite_hoe");
	}
	
	public void graphiteToolRegister() {
		NovaCraft.GraphitePickaxe = new NItemPickaxe(NovaCraft.GraphiteTool, "graphite_pickaxe");
		NovaCraft.GraphiteAxe = new NItemAxe(NovaCraft.GraphiteTool, "graphite_axe");
		NovaCraft.GraphiteShovel = new NItemShovel(NovaCraft.GraphiteTool, "graphite_shovel");
		NovaCraft.GraphiteHoe = new NItemHoe(NovaCraft.GraphiteTool, "graphite_hoe");
		
		GameRegistry.registerItem(NovaCraft.GraphitePickaxe, "graphite_pickaxe");
		GameRegistry.registerItem(NovaCraft.GraphiteAxe, "graphite_axe");
		GameRegistry.registerItem(NovaCraft.GraphiteShovel, "graphite_shovel");
		GameRegistry.registerItem(NovaCraft.GraphiteHoe, "graphite_hoe");
	}
	
}
