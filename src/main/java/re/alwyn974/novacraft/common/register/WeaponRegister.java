/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.register;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.potion.Potion;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.item.weapon.NItemBattleAxe;
import re.alwyn974.novacraft.item.weapon.NItemBattleHammer;
import re.alwyn974.novacraft.item.weapon.NItemBow;
import re.alwyn974.novacraft.item.weapon.NItemSword;

public class WeaponRegister {
	
	public WeaponRegister() {
		bowRegister();
		swordRegsiter();
		battleAxeRegister();
		battleHammerRegister();
	}
	
	public void bowRegister() {
		//NovaCraft.DiamondBow = new NItemBow(384, "diamond_bow", Items.diamond);
		NovaCraft.InsidiumBow = new NItemBow(445, "insidium_bow", NovaCraft.Insidium, 5.0F, Potion.moveSlowdown, Potion.weakness, Potion.blindness);
		NovaCraft.SpinelleBow = new NItemBow(430, "spinelle_bow", NovaCraft.Spinelle, 4.0F, Potion.blindness);
		NovaCraft.MicroliteBow = new NItemBow(415, "microlite_bow", NovaCraft.Microlite, 3.3333333F, Potion.weakness);
		NovaCraft.GraphiteBow = new NItemBow(400, "graphite_bow", NovaCraft.Graphite, 3.0F, Potion.moveSlowdown);
		
		//GameRegistry.registerItem(NovaCraft.DiamondBow, "diamond_bow");
		GameRegistry.registerItem(NovaCraft.InsidiumBow, "insidium_bow");
		GameRegistry.registerItem(NovaCraft.MicroliteBow, "microlite_bow");
		GameRegistry.registerItem(NovaCraft.GraphiteBow, "graphite_bow");
		GameRegistry.registerItem(NovaCraft.SpinelleBow, "spinelle_bow");
	}
	
	public void swordRegsiter() {
		NovaCraft.InsidiumSword = new NItemSword(NovaCraft.InsidiumTool, "insidium_sword");
		NovaCraft.SpinelleSword = new NItemSword(NovaCraft.SpinelleTool, "spinelle_sword");
		NovaCraft.MicroliteSword = new NItemSword(NovaCraft.MicroliteTool, "microlite_sword");
		NovaCraft.GraphiteSword = new NItemSword(NovaCraft.GraphiteTool, "graphite_sword");
		
		GameRegistry.registerItem(NovaCraft.InsidiumSword, "insidium_sword");
		GameRegistry.registerItem(NovaCraft.SpinelleSword, "spinelle_sword");
		GameRegistry.registerItem(NovaCraft.MicroliteSword, "microlite_sword");
		GameRegistry.registerItem(NovaCraft.GraphiteSword, "graphite_sword");
	}
	
	public void battleAxeRegister() {
		NovaCraft.InsidiumBattleAxe = new NItemBattleAxe(NovaCraft.InsidiumTool, "insidium_battle_axe", 2500);
		NovaCraft.SpinelleBattleAxe = new NItemBattleAxe(NovaCraft.SpinelleTool, "spinelle_battle_axe", 2000);
		//NovaCraft.MicroliteBattleAxe = new NItemBattleAxe(NovaCraft.MicroliteTool, "microlite_battle_axe", 1800);
		//NovaCraft.GraphiteBattleAxe = new NItemBattleAxe(NovaCraft.GraphiteTool, "graphite_battle_axe", 1600);
	
		GameRegistry.registerItem(NovaCraft.InsidiumBattleAxe, "insidium_battle_axe");
		GameRegistry.registerItem(NovaCraft.SpinelleBattleAxe, "spinelle_battle_axe");
		//GameRegistry.registerItem(NovaCraft.MicroliteBattleAxe, "microlite_battle_axe");
		//GameRegistry.registerItem(NovaCraft.GraphiteBattleAxe, "graphite_battle_axe");
	}
	
	public void battleHammerRegister() {
		NovaCraft.InsidiumBattleHammer = new NItemBattleHammer(NovaCraft.InsidiumTool, "insidium_battle_hammer", 2500);
		NovaCraft.SpinelleBattleHammer = new NItemBattleHammer(NovaCraft.SpinelleTool, "spinelle_battle_hammer", 2000);
		//NovaCraft.MicroliteBattleHammer = new NItemBattleHammer(NovaCraft.MicroliteTool, "microlite_battle_hammer", 1800);
		//NovaCraft.GraphiteBattleHammer = new NItemBattleHammer(NovaCraft.GraphiteTool, "graphite_battle_hammer", 1600);
		
		GameRegistry.registerItem(NovaCraft.InsidiumBattleHammer, "insidium_battle_hammer");
		GameRegistry.registerItem(NovaCraft.SpinelleBattleHammer, "spinelle_battle_hammer");
		//GameRegistry.registerItem(NovaCraft.MicroliteBattleHammer, "microlite_battle_hammer");
		//GameRegistry.registerItem(NovaCraft.GraphiteBattleHammer, "graphite_battle_hammer");
	}

}
