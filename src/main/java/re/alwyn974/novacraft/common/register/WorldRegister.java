/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.common.register;

import cpw.mods.fml.common.IWorldGenerator;
import cpw.mods.fml.common.registry.GameRegistry;
import re.alwyn974.novacraft.world.gen.NovaCraftDecorationGenerator;
import re.alwyn974.novacraft.world.gen.NovaCraftOreGenerator;

public class WorldRegister {
	
	public static void mainRegistry() {	
		registerWorldGen(new NovaCraftOreGenerator(), 0);
		registerWorldGen(new NovaCraftDecorationGenerator(), 0);
	}
	
	public static void registerWorldGen(IWorldGenerator iGenerator, int propability) {
		GameRegistry.registerWorldGenerator(iGenerator, propability);
	}

}
