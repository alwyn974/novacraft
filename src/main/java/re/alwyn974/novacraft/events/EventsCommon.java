/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.events;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAnvil;
import net.minecraft.block.BlockEnchantmentTable;
import net.minecraft.block.BlockObsidian;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.ChunkPosition;
import net.minecraft.world.Explosion;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.world.ExplosionEvent;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.block.BlockReinforcedObsidian;
import re.alwyn974.novacraft.common.entity.NEntityArrow;
import re.alwyn974.novacraft.item.weapon.NItemBattleHammer;

public class EventsCommon {

	public EventsCommon() {
		FMLCommonHandler.instance().bus().register(this);
		MinecraftForge.EVENT_BUS.register(this);
	}

	@SubscribeEvent
	public void onTntExplode(ExplosionEvent.Detonate e) {
		for (ChunkPosition pos : getBlocks((int) e.explosion.explosionSize / 2, e.explosion)) {
			int x = pos.chunkPosX;
			int y = pos.chunkPosY;
			int z = pos.chunkPosZ;
			Block b = e.world.getBlock(x, y, z);
			
			if (!e.explosion.exploder.worldObj.handleMaterialAcceleration(e.explosion.exploder.boundingBox.expand(0.0D, -0.4000000059604645D, 0.0D).contract(0.001D, 0.001D, 0.001D), Material.water, e.explosion.exploder)) {
				if (b instanceof BlockObsidian || b instanceof BlockEnchantmentTable || b instanceof BlockAnvil || b == NovaCraft.ObsidianStairs
						|| b == NovaCraft.ObsidianFence || b == NovaCraft.ObsidianSlab || b == NovaCraft.ObsidianDoubleSlab) {
					if (e.world.getBlockMetadata(x, y, z) >= 6 || b == NovaCraft.ObsidianStairs)
						e.getAffectedBlocks().add(pos);
					else if (e.world.rand.nextInt(2) <= 1)
						e.world.setBlockMetadataWithNotify(x, y, z, e.world.getBlockMetadata(x, y, z) + 1, 2);
				} else if (b instanceof BlockReinforcedObsidian) {
					if (e.world.getBlockMetadata(x, y, z) >= 12)
						e.getAffectedBlocks().add(pos);
					else if (e.world.rand.nextInt(2) <= 1)
						e.world.setBlockMetadataWithNotify(x, y, z, e.world.getBlockMetadata(x, y, z) + 1, 2);
				}
			}			
		}
	}

	public List<ChunkPosition> getBlocks(int size, Explosion e) {
		ArrayList<ChunkPosition> pos = new ArrayList<ChunkPosition>();
		for (int x = -size; x < size; x++) {
			for (int y = -size; y < size; y++) {
				for (int z = -size; z < size; z++) {
					pos.add(new ChunkPosition((int) e.explosionX + x, (int) e.explosionY + y, (int) e.explosionZ + z));
				}
			}
		}
		return pos;
	}
	
	@SubscribeEvent
	public void LivingHurtEvent(LivingHurtEvent e) {
		if (e.entity != null && e.source != null && e.source.getSourceOfDamage() != null) {

			if (e.source.getSourceOfDamage() instanceof EntityPlayer && ((EntityLivingBase) e.source.getSourceOfDamage()).getHeldItem() != null) {
				ItemStack dmgSource = ((EntityLivingBase) e.source.getSourceOfDamage()).getHeldItem();
				if (e.entityLiving instanceof EntityPlayer && e.source.damageType.equals("player") && dmgSource != null && (dmgSource.getItem() instanceof NItemBattleHammer)) {
					
					EntityPlayer player = (EntityPlayer) e.entityLiving;
					ItemStack boots = player.getEquipmentInSlot(1);
					ItemStack leggings = player.getEquipmentInSlot(2);
					ItemStack chestplate = player.getEquipmentInSlot(3);
					ItemStack helmet = player.getEquipmentInSlot(4);
					int damage = e.entity.worldObj.rand.nextInt(5);
					
					if (boots != null) {
						int unbreakingLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.unbreaking.effectId, boots);
						if (unbreakingLevel == 2)
							damage = e.entity.worldObj.rand.nextInt(4);
						else if (unbreakingLevel == 3)
							damage = e.entity.worldObj.rand.nextInt(3);
						boots.damageItem(damage, player);
					} else if (leggings != null) {
						int unbreakingLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.unbreaking.effectId, leggings);
						if (unbreakingLevel == 2)
							damage = e.entity.worldObj.rand.nextInt(4);
						else if (unbreakingLevel == 3)
							damage = e.entity.worldObj.rand.nextInt(3);
						leggings.damageItem(damage, player);
					} else if (chestplate != null) {
						int unbreakingLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.unbreaking.effectId, chestplate);
						if (unbreakingLevel == 2)
							damage = e.entity.worldObj.rand.nextInt(4);
						else if (unbreakingLevel == 3)
							damage = e.entity.worldObj.rand.nextInt(3);
						chestplate.damageItem(damage, player);
					} else if (helmet != null) {
						int unbreakingLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.unbreaking.effectId, leggings);
						if (unbreakingLevel == 2)
							damage = e.entity.worldObj.rand.nextInt(4);
						else if (unbreakingLevel == 3)
							damage = e.entity.worldObj.rand.nextInt(3);
						helmet.damageItem(damage, player);
					}
				}
			} 
			
			else if (e.source.getSourceOfDamage() instanceof NEntityArrow) {
				NEntityArrow arrow = (NEntityArrow) e.source.getSourceOfDamage();
				if (arrow.getEffects() == null)
					return;
				else {
					for (Potion potion : arrow.getEffects()) {
						e.entityLiving.addPotionEffect(new PotionEffect(potion.id, 20 * 5, 1));
					}
				}
				
			}
		}
	}
	
}
