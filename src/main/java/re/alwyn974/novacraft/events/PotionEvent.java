/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.events;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockObsidian;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;
import net.minecraft.util.Vec3;
import net.minecraft.world.ChunkPosition;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import re.alwyn974.novacraft.NovaCraft;

public class PotionEvent {

	public PotionEvent() {
		FMLCommonHandler.instance().bus().register(this);
		MinecraftForge.EVENT_BUS.register(this);
	}

	@SubscribeEvent
	public void onEntityUpdate(LivingUpdateEvent e) {
		if (e.entityLiving == null)
			return;
		tickSlowfall(e);
		if (FMLCommonHandler.instance().getSide().isClient())
			tickView(e);		
	}

	public void tickSlowfall(LivingUpdateEvent e) {
		if (e.entityLiving.isPotionActive(NovaCraft.SlowFall)) {
			if (e.entityLiving instanceof EntityPlayer) {
				EntityPlayer p = (EntityPlayer) e.entityLiving;
				if (p.isSneaking()) {
					return;
				}
			}
			if (e.entityLiving.motionY < 0.0D) {
				e.entityLiving.motionY *= 0.41F;
				e.entityLiving.fallDistance = 0.0F;
			}
		}
	}
	
	List<ChunkPosition> blocksPos = new ArrayList<ChunkPosition>();
	@SideOnly(Side.CLIENT)
	public void tickView(LivingUpdateEvent e) {
 		if (e.entityLiving instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) e.entityLiving;
			Minecraft mc = Minecraft.getMinecraft();
			MovingObjectPosition mop = mc.objectMouseOver;
			if (mop != null && mop.typeOfHit == MovingObjectType.BLOCK) {
				int x = mop.blockX;
				int y = mop.blockY;
				int z = mop.blockZ;

				Vec3 lookVec = player.getLookVec();
				double xLook = Math.abs(lookVec.xCoord);
				double yLook = Math.abs(lookVec.yCoord);
				double zLook = Math.abs(lookVec.zCoord);

				double max = Math.max(xLook, Math.max(yLook, zLook));

				int addX = 1;
				int addY = 1;
				int addZ = 1;

				if (max == xLook) {
					y -= 1;
					z -= 1;
					addX = 3;
				} else if (max == yLook) {
					x -= 1;
					z -= 1;
					addY = 3;
				} else if (max == zLook) {
					x -= 1;
					y -= 1;
					addZ = 3;
				}

				if (player.isPotionActive(NovaCraft.ViewEffect) && player.getActivePotionEffect(NovaCraft.ViewEffect).getDuration() > 20) {
					for (int xOffset = 0; xOffset < 3; xOffset += addX) {
						for (int yOffset = 0; yOffset < 3; yOffset += addY) {
							for (int zOffset = 0; zOffset < 3; zOffset += addZ) {
								if (player.worldObj.getBlock(x + xOffset, y + yOffset, z + zOffset) != null) {
									Block block = player.worldObj.getBlock(x + xOffset, y + yOffset, z + zOffset);
									if (block instanceof BlockObsidian) {
										player.worldObj.setBlock(x + xOffset, y + yOffset, z + zOffset, NovaCraft.CaveBlock);
										blocksPos.add(new ChunkPosition(x + xOffset, y + yOffset, z + zOffset));
									}
								}
							}
						}
					}
				} else if (player.isPotionActive(NovaCraft.ViewEffect) && player.getActivePotionEffect(NovaCraft.ViewEffect).getDuration() <= 20) {
					for (ChunkPosition pos : blocksPos) {
						int blockX = pos.chunkPosX;
						int blockY = pos.chunkPosY;
						int blockZ = pos.chunkPosZ;
						player.worldObj.setBlock(blockX, blockY, blockZ, Blocks.obsidian);
					}
				}
			}
		}
	}

}
