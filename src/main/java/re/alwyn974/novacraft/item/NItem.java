/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import re.alwyn974.novacraft.NovaCraft;

public class NItem extends Item {

	public NItem(String name) {
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setUnlocalizedName(name);
		this.setTextureName(NovaCraft.TEXTURE_ID + name);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean inHand) {
		super.addInformation(stack, player, list, inHand);
		if (this == NovaCraft.Graphite) {
			list.add("   §9Insidium");
			list.add("   §4Spinelle");
			list.add("   §6Microlite");
			list.add("§e➳ §8Graphite");
		} else if (this == NovaCraft.Microlite) {
			list.add("   §9Insidium");
			list.add("   §4Spinelle");
			list.add("§e➳ §6Microlite");
			list.add("   §8Graphite");
		} else if (this == NovaCraft.Spinelle) {
			list.add("   §9Insidium");
			list.add("§e➳ §4Spinelle");
			list.add("   §6Microlite");
			list.add("   §8Graphite");
		} else if (this == NovaCraft.Insidium) {
			list.add("§e➳ §9Insidium");
			list.add("   §4Spinelle");
			list.add("   §6Microlite");
			list.add("   §8Graphite");
		}
	}

}
