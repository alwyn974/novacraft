/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import re.alwyn974.novacraft.NovaCraft;

public class NItemArmor extends ItemArmor {

	private Item repairItem;

	public NItemArmor(ArmorMaterial material, int type, String name, Item repairItem) {
		super(material, 0, type);
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setUnlocalizedName(name);
		this.setTextureName(NovaCraft.TEXTURE_ID + name);
		this.repairItem = repairItem;
	}

	@Override
	public boolean getIsRepairable(ItemStack input, ItemStack repair) {
		if (input.getItem() == this && repair.getItem() == repairItem) 
			return true;
		return super.getIsRepairable(input, repair);
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
		if (slot == 2) {
			if (stack.getItem() == NovaCraft.GraphiteLeggings)
				return NovaCraft.TEXTURE_ID + "textures/models/armor/graphite_armor_layer_2.png";
			else if (stack.getItem() == NovaCraft.MicroliteLeggings)
				return NovaCraft.TEXTURE_ID + "textures/models/armor/microlite_armor_layer_2.png";
			else if (stack.getItem() == NovaCraft.SpinelleLeggings)
				return NovaCraft.TEXTURE_ID + "textures/models/armor/spinelle_armor_layer_2.png";
			else if (stack.getItem() == NovaCraft.InsidiumLeggings)
				return NovaCraft.TEXTURE_ID + "textures/models/armor/insidium_armor_layer_2.png";
		} else {
			if (stack.getItem() == NovaCraft.GraphiteHelmet || stack.getItem() == NovaCraft.GraphiteChestPlate || stack.getItem() == NovaCraft.GraphiteBoots)
				return NovaCraft.TEXTURE_ID + "textures/models/armor/graphite_armor_layer_1.png";
			else if (stack.getItem() == NovaCraft.MicroliteHelmet || stack.getItem() == NovaCraft.MicroliteChestPlate || stack.getItem() == NovaCraft.MicroliteBoots)
				return NovaCraft.TEXTURE_ID + "textures/models/armor/microlite_armor_layer_1.png";
			else if (stack.getItem() == NovaCraft.SpinelleHelmet || stack.getItem() == NovaCraft.SpinelleChestPlate || stack.getItem() == NovaCraft.SpinelleBoots)
				return NovaCraft.TEXTURE_ID + "textures/models/armor/spinelle_armor_layer_1.png";
			else if (stack.getItem() == NovaCraft.InsidiumHelmet || stack.getItem() == NovaCraft.InsidiumChestPlate || stack.getItem() == NovaCraft.InsidiumBoots)
				return NovaCraft.TEXTURE_ID + "textures/models/armor/insidium_armor_layer_1.png";
		}
		return "";
	}

	@Override
	public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack) {
		if (this == NovaCraft.InsidiumHelmet) 
			player.addPotionEffect(new PotionEffect(Potion.damageBoost.id, 10 * 20, 1));
		else if (this == NovaCraft.InsidiumChestPlate) 
			player.addPotionEffect(new PotionEffect(Potion.fireResistance.id, 10 * 20, 1));
		else if (this == NovaCraft.InsidiumLeggings) 
			player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 10 * 20, 1));
		else if (this == NovaCraft.InsidiumBoots) 
			player.addPotionEffect(new PotionEffect(Potion.jump.id, 10 * 20, 1));
	}

}
