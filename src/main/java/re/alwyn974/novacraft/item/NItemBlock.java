/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import re.alwyn974.novacraft.NovaCraft;

public class NItemBlock extends ItemBlock {

	public NItemBlock(Block block) {
		super(block);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean inHand) {
		super.addInformation(stack, player, list, inHand);
		if (stack.isItemEqual(new ItemStack(NovaCraft.GraphiteOre)) || stack.isItemEqual(new ItemStack(NovaCraft.GraphiteBlock))) {
			list.add("   §9Insidium");
			list.add("   §4Spinelle");
			list.add("   §6Microlite");
			list.add("§e➳ §8Graphite");
		} else if (stack.isItemEqual(new ItemStack(NovaCraft.MicroliteOre)) || stack.isItemEqual(new ItemStack(NovaCraft.MicroliteBlock))) {
			list.add("   §9Insidium");
			list.add("   §4Spinelle");
			list.add("§e➳ §6Microlite");
			list.add("   §8Graphite");
		} else if (stack.isItemEqual(new ItemStack(NovaCraft.SpinelleOre)) || stack.isItemEqual(new ItemStack(NovaCraft.SpinelleBlock))) {
			list.add("   §9Insidium");
			list.add("§e➳ §4Spinelle");
			list.add("   §6Microlite");
			list.add("   §8Graphite");
		} else if (stack.isItemEqual(new ItemStack(NovaCraft.InsidiumOre)) || stack.isItemEqual(new ItemStack(NovaCraft.InsidiumBlock))) {
			list.add("§e➳ §9Insidium");
			list.add("   §4Spinelle");
			list.add("   §6Microlite");
			list.add("   §8Graphite");
		} else if (stack.isItemEqual(new ItemStack(NovaCraft.GraphiteChest)))
			list.add("§eUn coffre comportant un double coffre.");
		else if (stack.isItemEqual(new ItemStack(NovaCraft.GraphiteLadder)))
			list.add("§ePermet d'aller 2 fois plus vite en montant des echelles.");
		else if (stack.isItemEqual(new ItemStack(NovaCraft.ReinforcedObsidian)))
			list.add("§eDe l'obsidienne pétable en 12 coups.");
		else if (stack.isItemEqual(new ItemStack(NovaCraft.GraphiteCrops)) || stack.isItemEqual(new ItemStack(NovaCraft.MicroliteCrops)) || stack.isItemEqual(new ItemStack(NovaCraft.SpinelleCrops)))
			list.add("§eCette plante ne peut que pousser dans le nether sur de la terre.");
	}

}
