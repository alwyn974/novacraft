/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item;

import net.minecraft.block.Block;
import net.minecraft.item.ItemSlab;
import re.alwyn974.novacraft.block.NBlockSlab;

public class NItemBlockSlab extends ItemSlab {

	public NItemBlockSlab(Block block, NBlockSlab singleSlab, NBlockSlab doubleSlab, Boolean stacked) {
		super(block, singleSlab, doubleSlab, stacked);
	}

}