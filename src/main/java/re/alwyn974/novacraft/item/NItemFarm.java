/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import re.alwyn974.novacraft.NovaCraft;

public class NItemFarm extends ItemFood {

	private int potionId;
	private int potionAmplifier;

	public NItemFarm(String name, int gigot, int saturation, boolean wolf) {
		super(gigot, saturation, wolf);
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setUnlocalizedName(name);
		this.setTextureName(NovaCraft.TEXTURE_ID + name);
		this.hasSubtypes = true;
	}

	@Override
	public boolean hasEffect(ItemStack stack) {
		return stack.getItemDamage() > 0;
	}

	/**
	 * Return an item rarity from EnumRarity
	 */
	public EnumRarity getRarity(ItemStack stack) {
		return stack.getItemDamage() == 0 ? EnumRarity.rare : EnumRarity.epic;
	}

	@Override
	protected void onFoodEaten(ItemStack stack, World world, EntityPlayer player) {
		if (!world.isRemote && this.potionId > 0 && stack.getItemDamage() == 0) {
			player.addPotionEffect(new PotionEffect(this.potionId, 3 * 60 * 20, this.potionAmplifier));
		}
	}

	public ItemFood setPotionEffect(int id, int amplifier) {
		this.potionId = id;
		this.potionAmplifier = amplifier;
		return this;
	}

	/**
	 * returns a list of items with the same ID, but different meta (eg: dye returns
	 * 16 items)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs tabs, List list) {
		list.add(new ItemStack(item, 1, 0));
		list.add(new ItemStack(item, 1, 1));
	}

}
