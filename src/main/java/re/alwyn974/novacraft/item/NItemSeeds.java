/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item;

import net.minecraft.block.Block;
import net.minecraft.item.ItemSeeds;
import re.alwyn974.novacraft.NovaCraft;

public class NItemSeeds extends ItemSeeds {

	public NItemSeeds(Block crop, Block farmland, String name) {
		super(crop, farmland);
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setUnlocalizedName(name);
		this.setTextureName(NovaCraft.TEXTURE_ID + name);
	}

}
