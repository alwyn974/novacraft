/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import re.alwyn974.novacraft.NovaCraft;

public class RodOfHeal extends Item {
	
	public RodOfHeal(String name) {
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setUnlocalizedName(name);
		this.setTextureName(NovaCraft.TEXTURE_ID + name);
		this.maxStackSize = 1;
		this.setMaxDamage(6);
	}
	
	@Override
	public boolean getIsRepairable(ItemStack input, ItemStack repair) {
		return (input.getItem() == this) && (repair.getItem() == this);
	}

	@Override
	public void onUpdate(ItemStack stack, World world, Entity entity, int paramInt, boolean inHand) {
		if (((entity instanceof EntityPlayer)) && (!world.isRemote)) {
			if (!stack.hasTagCompound()) {
				stack.setTagCompound(new NBTTagCompound());
				stack.getTagCompound().setInteger("timer", 0);
			}
			if ((stack.getTagCompound().getInteger("timer") > 0)
					&& (stack.getTagCompound().hasKey("timer"))) {
				int i = stack.getTagCompound().getInteger("timer") - 1;
				stack.getTagCompound().setInteger("timer", i);
			}
		}
	}

	@Override
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
		if ((stack.getTagCompound().getInteger("timer") == 0) && (!world.isRemote)) {
			player.heal(12.0F);
			stack.damageItem(1, player);
			stack.getTagCompound().setInteger("timer", 20 * 20);
		}
		return stack;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean inHand) {
		super.addInformation(stack, player, list, inHand);
		list.add("§ePermet de régénérer 6 coeurs par utilisation");
		if ((stack.hasTagCompound()) && (stack.getTagCompound().hasKey("timer"))) {
			list.add("§3> Temps avant le prochain Heal :");
			list.add("§e> " + stack.getTagCompound().getInteger("timer") / 20 + " secondes");
		}
	}
}
