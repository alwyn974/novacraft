/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item.tools;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import re.alwyn974.novacraft.NovaCraft;

public class NItemAxe extends ItemAxe {

	public NItemAxe(ToolMaterial material, String name) {
		super(material);
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setUnlocalizedName(name);
		this.setTextureName(NovaCraft.TEXTURE_ID + name);
	}

	@Override
	public boolean onBlockDestroyed(ItemStack stack, World world, Block block, int x, int y, int z, EntityLivingBase living) {
		if (this == NovaCraft.InsidiumAxe) {
			for (int i = y; i < 256; i++) {
				if (world.getBlock(x, i, z).isWood(world, x, i, z)) {
					stack.damageItem(1, living);
					if (!world.isRemote && world.getGameRules().getGameRuleBooleanValue("doTileDrops")) {
						float f = 0.7F;
						double d0 = (double) (world.rand.nextFloat() * f) + (double) (1.0F - f) * 0.5D;
						double d1 = (double) (world.rand.nextFloat() * f) + (double) (1.0F - f) * 0.5D;
						double d2 = (double) (world.rand.nextFloat() * f) + (double) (1.0F - f) * 0.5D;
						EntityItem entityitem = new EntityItem(world, (double) x + d0, (double) i + d1, (double) z + d2, new ItemStack(world.getBlock(x, i, z), 1, world.getBlockMetadata(x, i, z)));
						entityitem.delayBeforeCanPickup = 10;
						world.spawnEntityInWorld(entityitem);
					}
					world.setBlockToAir(x, i, z);
				} else {
					return super.onBlockDestroyed(stack, world, block, x, i, z, living);
				}
			}
		}
		return super.onBlockDestroyed(stack, world, block, x, y, z, living);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean inHand) {
		super.addInformation(stack, player, list, inHand);
		if (this == NovaCraft.InsidiumAxe)
			list.add("§ePermet de casser l'arbre en entier");
	}

}
