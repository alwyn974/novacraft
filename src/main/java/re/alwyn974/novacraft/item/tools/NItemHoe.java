/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item.tools;

import cpw.mods.fml.common.eventhandler.Event.Result;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.UseHoeEvent;
import re.alwyn974.novacraft.NovaCraft;

public class NItemHoe extends ItemHoe {

	public NItemHoe(ToolMaterial material, String name) {
		super(material);
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setUnlocalizedName(name);
		this.setTextureName(NovaCraft.TEXTURE_ID + name);
	}
	
	@Override
	public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
		if (!player.isSneaking() && this == NovaCraft.InsidiumHoe) {
			for (int i = x - 2; i <= x + 2; i++) {
				for (int j = z - 2; j <= z + 2; j++) {
					useHoe(stack, player, world, i, y, j, side);
				}
			}
			return true;
		} 
		return super.onItemUse(stack, player, world, x, y, z, side, hitX, hitY, hitZ);
	}
	
	public boolean useHoe(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side) {
		if (!player.canPlayerEdit(x, y, z, side, stack))
			return false;
		else {
			UseHoeEvent event = new UseHoeEvent(player, stack, world, x, y, z);
			if (MinecraftForge.EVENT_BUS.post(event))
				return false;
			else if (event.getResult() == Result.ALLOW) {
				stack.damageItem(1, player);
				return true;
			}
			Block block = world.getBlock(x, y, z);
			boolean air = world.isAirBlock(x, y + 1, z);
			if (side != 0 && air && (block == Blocks.grass || block == Blocks.dirt)) {
				Block farmland = Blocks.farmland;
				world.playSoundEffect(x + 0.5F, y + 0.5F, z + 0.5F, farmland.stepSound.getStepResourcePath(),
						(farmland.stepSound.getVolume() + 1.0F) / 2.0F, farmland.stepSound.getPitch() * 0.8F);
				if (world.isRemote)
					return true;
				else {
					world.setBlock(x, y, z, farmland);
					stack.damageItem(1, player);
					return true;
				}
			} else
				return false;
		}
	}

}
