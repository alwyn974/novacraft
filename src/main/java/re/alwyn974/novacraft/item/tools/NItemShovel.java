/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item.tools;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import re.alwyn974.novacraft.NovaCraft;

public class NItemShovel extends ItemSpade {

	public NItemShovel(ToolMaterial material, String name) {
		super(material);
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setUnlocalizedName(name);
		this.setTextureName(NovaCraft.TEXTURE_ID + name);
	}
	
	@Override
	public void onCreated(ItemStack stack, World world, EntityPlayer player) {
		if (this == NovaCraft.InsidiumShovel)
			stack.addEnchantment(Enchantment.silkTouch, 1);
	}
	
}
