/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item.weapon;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.item.ItemSword;
import re.alwyn974.novacraft.NovaCraft;

public class NItemBattleAxe extends ItemSword {

	private float damage;

	public NItemBattleAxe(ToolMaterial material, String name, int durability) {
		super(material);
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setUnlocalizedName(name);
		this.setTextureName(NovaCraft.TEXTURE_ID + name);
		this.setMaxDamage(durability);
		this.damage = 5.0F + material.getDamageVsEntity();
	}

	@Override
	public Multimap<String, AttributeModifier> getItemAttributeModifiers() {
		Multimap<String, AttributeModifier> multimap = HashMultimap.create();
		multimap.put(SharedMonsterAttributes.attackDamage.getAttributeUnlocalizedName(),
				new AttributeModifier(field_111210_e, "Weapon modifier", (double) this.damage, 0));
		return multimap;
	}

}
