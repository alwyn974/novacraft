/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item.weapon;

import java.util.List;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import re.alwyn974.novacraft.NovaCraft;

public class NItemBattleHammer extends ItemSword {

	private float damage;

	public NItemBattleHammer(ToolMaterial material, String name, int durability) {
		super(material);
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setUnlocalizedName(name);
		this.setTextureName(NovaCraft.TEXTURE_ID + name);
		this.setMaxDamage(durability);
		this.damage = 3.0F + material.getDamageVsEntity();
	}

	@Override
	public void onUpdate(ItemStack stack, World world, Entity entity, int slotIndex, boolean inHand) {
		if (inHand && !world.isRemote) {
			((EntityPlayer)entity).addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 10 * 20, 1));
		}
	}
	
	@Override
	public Multimap<String, AttributeModifier> getItemAttributeModifiers() {
		Multimap<String, AttributeModifier> multimap = HashMultimap.create();
		multimap.put(SharedMonsterAttributes.attackDamage.getAttributeUnlocalizedName(), new AttributeModifier(field_111210_e, "Weapon modifier", (double) this.damage, 0));
		return multimap;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean inHand) {
		super.addInformation(stack, player, list, inHand);
		list.add("§ePermet de casser l'armure de l'adversaire plus rapidement");
	}

}
