/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item.weapon;

import java.util.List;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.ArrowLooseEvent;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.common.entity.NEntityArrow;

public class NItemBow extends ItemBow {

	public static final String[] bowPullIconNameArray = new String[] { "0", "1", "2" };
	private Item repairItem;
	private float damage = 2.0F;
	private Potion[] effects;
	@SideOnly(Side.CLIENT)
	private IIcon[] iconArray;
	
	public NItemBow(int durability, String name, Item repairItem) {
		this(durability, name, repairItem, 0);
	}
	
	public NItemBow(int durability, String name, Item repairItem, float damage, Potion... effects) {
		this.maxStackSize = 1;
		this.repairItem = repairItem;
		this.damage = damage;
		this.effects = effects;
		this.setMaxDamage(durability);
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setUnlocalizedName(name);
		this.setTextureName(NovaCraft.TEXTURE_ID + name);
	}
	
	@Override
	public boolean getIsRepairable(ItemStack input, ItemStack repair) {
		if (input.getItem() == this && (repair.getItem() == repairItem) || (repair.getItem() == this)) {
			return true;
		}
		return false;
	}
	
	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World world, EntityPlayer player, int par4) {
		int j = this.getMaxItemUseDuration(stack) - par4;

		ArrowLooseEvent event = new ArrowLooseEvent(player, stack, j);
		MinecraftForge.EVENT_BUS.post(event);
		if (event.isCanceled()) {
			return;
		}
		j = event.charge;

		boolean flag = player.capabilities.isCreativeMode || EnchantmentHelper.getEnchantmentLevel(Enchantment.infinity.effectId, stack) > 0;

		if (flag || player.inventory.hasItem(Items.arrow)) {
			float f = (float) j / 20.0F;
			f = (f * f + f * 2.0F) / 3.0F;

			if ((double) f < 0.1D) {
				return;
			}

			if (f > 1.0F) {
				f = 1.0F;
			}
			
			NEntityArrow entityarrow = new NEntityArrow(world, player, f * damage, effects);

			if (f == 1.0F) {
				entityarrow.setIsCritical(true);
			}

			int k = EnchantmentHelper.getEnchantmentLevel(Enchantment.power.effectId, stack);

			if (k > 0) {
				entityarrow.setDamage(entityarrow.getDamage() + (double) k * 0.5D + 0.5D);
			}

			int l = EnchantmentHelper.getEnchantmentLevel(Enchantment.punch.effectId, stack);

			if (l > 0) {
				entityarrow.setKnockbackStrength(l);
			}

			if (EnchantmentHelper.getEnchantmentLevel(Enchantment.flame.effectId, stack) > 0) {
				entityarrow.setFire(100);
			}

			stack.damageItem(1, player);
			world.playSoundAtEntity(player, "random.bow", 1.0F, 1.0F / (itemRand.nextFloat() * 0.4F + 1.2F) + f * 0.5F);

			if (flag) {
				entityarrow.canBePickedUp = 2;
			} else {
				player.inventory.consumeInventoryItem(Items.arrow);
			}

			if (!world.isRemote) {
				world.spawnEntityInWorld(entityarrow);
			}
		}
	}
	
	@Override
	public boolean isFull3D() {
		return true;
	}
	
	@Override
	public int getMaxItemUseDuration(ItemStack stack) {
		return 80000;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IIconRegister IconRegister) {
		this.itemIcon = IconRegister.registerIcon(this.getIconString());
		this.iconArray = new IIcon[bowPullIconNameArray.length];

		for (int i = 0; i < this.iconArray.length; ++i) {
			this.iconArray[i] = IconRegister.registerIcon(this.getIconString() + "_" + bowPullIconNameArray[i]);
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public IIcon getIcon(ItemStack stack, int renderPass, EntityPlayer player, ItemStack usingItem, int useRemaining) {
		if (Minecraft.getMinecraft().gameSettings.thirdPersonView != 0) {
			GL11.glTranslatef(0.0F,  -0.6F, -0.025F);
			GL11.glRotatef(-17.0F, 0.0F, 0.0F, 1.0F);
			GL11.glRotatef(14.0F, 1.0F, 0.0F, 0.0F);
			GL11.glRotatef(4.5F, 0.0F, 1.0F, 0.0F);
		}
		
		if (usingItem != null && usingItem.getItem().equals(this)) {
			int k = usingItem.getMaxItemUseDuration() - useRemaining;
			if (k >= 18)
				return iconArray[2];
			if (k > 13)
				return iconArray[1];
			if (k > 0)
				return iconArray[0];
		}
		return getIconIndex(stack);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean inHand) {
		super.addInformation(stack, player, list, inHand);
		if (this == NovaCraft.SpinelleBow)
			list.add("§eDonne un effet de cécité à l'ennemie pendant 5s.");
		else if (this == NovaCraft.GraphiteBow)
			list.add("§eDonne un effet de ralentissement à l'ennemie pendant 5s.");
		else if (this == NovaCraft.MicroliteBow)
			list.add("§eDonne un effet de Faiblesse 2 à l'enemie pendant 5s.");
		else if (this == NovaCraft.InsidiumBow)
			list.add("§eDonne un effet de cécité, ralentissement 2 et faiblesse 2 à l'enemie pendant 5s.");
	}
	
}
