/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.item.weapon;

import net.minecraft.item.ItemSword;
import re.alwyn974.novacraft.NovaCraft;

public class NItemSword extends ItemSword {
	
	public NItemSword(ToolMaterial material, String name) {
		super(material);
		this.setCreativeTab(NovaCraft.NovaCraftCreativeTabs);
		this.setUnlocalizedName(name);
		this.setTextureName(NovaCraft.TEXTURE_ID + name);
	}

}
