/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.network.packet;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentTranslation;
import re.alwyn974.novacraft.network.proxy.ClientProxy;

public class PacketShowInvisibleBlock implements IMessage {

	@Override
	public void fromBytes(ByteBuf buf) {}

	@Override
	public void toBytes(ByteBuf buf) {}
	
	public static class Handler implements IMessageHandler<PacketShowInvisibleBlock, IMessage> {

		@SideOnly(Side.CLIENT)
		@Override
		public IMessage onMessage(PacketShowInvisibleBlock message, MessageContext ctx) {
			ClientProxy.insivisibleBlock = !ClientProxy.insivisibleBlock;
			EntityPlayer p = Minecraft.getMinecraft().thePlayer;
			
			if (ClientProxy.insivisibleBlock)
				p.addChatMessage(new ChatComponentTranslation("message.novacraft.command.hide"));
			else 
				p.addChatMessage(new ChatComponentTranslation("message.novacraft.command.show"));
			
			int rad = 20;
			int x = (int) p.posX;
			int y = (int) p.posY;
			int z = (int) p.posZ;
			p.worldObj.markBlockRangeForRenderUpdate(x - rad, y - rad, z - rad, x + rad, y + rad, z + rad);
		
			return null;
		}
		
	}


}
