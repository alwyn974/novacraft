/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.network.proxy;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.client.renderer.entity.RenderSnowball;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.block.entity.TileEntityGraphiteChest;
import re.alwyn974.novacraft.client.render.GraphiteChestInventoryRender;
import re.alwyn974.novacraft.client.render.TileEntityGraphiteChestRenderer;
import re.alwyn974.novacraft.common.entity.EntityPetard;

public class ClientProxy extends CommonProxy {
	
	public static int graphiteChestRenderId;
	public static boolean insivisibleBlock = true;

	public void registerRender() {
		RenderingRegistry.registerEntityRenderingHandler(EntityPetard.class, new RenderSnowball(NovaCraft.Petard));
		graphiteChestRenderId = RenderingRegistry.getNextAvailableRenderId();
        RenderingRegistry.registerBlockHandler(graphiteChestRenderId, new GraphiteChestInventoryRender());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityGraphiteChest.class, new TileEntityGraphiteChestRenderer());
	}

}
