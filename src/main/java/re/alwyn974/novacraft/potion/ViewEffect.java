/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.potion;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.potion.Potion;
import net.minecraft.util.ResourceLocation;
import re.alwyn974.novacraft.NovaCraft;

public class ViewEffect extends Potion {
	
	public static final ResourceLocation icon = new ResourceLocation(NovaCraft.MODID, "textures/gui/inventory.png");
	
	public ViewEffect(int id, Boolean badEffect, int color, String name) {
		super(id, badEffect, color);
		this.setPotionName("potion." + name);
		setIconIndex(1, 0);
	}

	public Potion setIconIndex(int x, int y) {
		super.setIconIndex(x, y);
		return this;
	}
	
	@SideOnly(Side.CLIENT)
	public int getStatusIconIndex() {
		Minecraft.getMinecraft().renderEngine.bindTexture(icon);
		return super.getStatusIconIndex();
	}

}
