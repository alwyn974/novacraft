/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.server.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import re.alwyn974.novacraft.NovaCraft;
import re.alwyn974.novacraft.network.packet.PacketShowInvisibleBlock;

public class CommandShow extends CommandBase implements ICommand {

	@Override
	public String getCommandName() {
		return "show";
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return "/show";
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		if (sender instanceof EntityPlayerMP) {
			EntityPlayerMP p = (EntityPlayerMP) sender;
			NovaCraft.network.sendTo(new PacketShowInvisibleBlock(), p);
		} else 
			sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Vous ne pouvez pas exécuter cette commande."));		
	}
	
	@Override
	public int getRequiredPermissionLevel() {
		return 2;
	}
	
}
