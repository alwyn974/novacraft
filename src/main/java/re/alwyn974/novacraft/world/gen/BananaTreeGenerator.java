/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.world.gen;

import java.util.Random;

import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import re.alwyn974.novacraft.NovaCraft;

public class BananaTreeGenerator extends WorldGenerator {

	public BananaTreeGenerator(boolean doBlockNotify) {
		super(doBlockNotify);
	}

	@Override
	public boolean generate(World world, Random rand, int x, int y, int z) {
		System.out.println(world.getBlock(x, y - 1, z));
		for (int i = 0; i < 4; i++) {
			world.setBlock(x, y + i, z, Blocks.log);
			world.setBlockMetadataWithNotify(x, y + i, z, 3, 2);
		}
		
		for (int i = 4; i < 7; i++) {
			world.setBlock(x, y + i, z, NovaCraft.BananaLeaves);
			world.setBlock(x + 1, y + i, z, NovaCraft.BananaLeaves);
			world.setBlock(x - 1, y + i, z, NovaCraft.BananaLeaves);
			world.setBlock(x, y + i, z + 1, NovaCraft.BananaLeaves);
			world.setBlock(x, y + i, z - 1, NovaCraft.BananaLeaves);
		}
		world.setBlock(x - 1, y + 5, z + 1, NovaCraft.BananaLeaves);
		world.setBlock(x + 1, y + 5, z + 1, NovaCraft.BananaLeaves);
		world.setBlock(x + 1, y + 5, z - 1, NovaCraft.BananaLeaves);
		world.setBlock(x - 1, y + 5, z - 1, NovaCraft.BananaLeaves);
		
		for (int i = 5; i < 7; i++) {
			world.setBlock(x + 2, y + i, z, NovaCraft.BananaLeaves);
			world.setBlock(x - 2, y + i, z, NovaCraft.BananaLeaves);
			world.setBlock(x, y + i, z + 2, NovaCraft.BananaLeaves);
			world.setBlock(x, y + i, z - 2, NovaCraft.BananaLeaves);
		}
		
		for (int i = 6; i < 8; i++) {
			world.setBlock(x + 3, y + i, z, NovaCraft.BananaLeaves);
			world.setBlock(x - 3, y + i, z, NovaCraft.BananaLeaves);
			world.setBlock(x, y + i, z + 3, NovaCraft.BananaLeaves);
			world.setBlock(x, y + i, z - 3, NovaCraft.BananaLeaves);
		}
		
		System.out.printf("généré à %d %d %d\n", x, y, z);
		
		return true;
	}

}
