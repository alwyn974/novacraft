/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.world.gen;

import java.util.Random;

import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.chunk.IChunkProvider;

public class NovaCraftDecorationGenerator implements IWorldGenerator {

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		switch (world.provider.dimensionId) {
		case 0:
			this.generateSurface(world, chunkX * 16, chunkZ * 16, random);
			break;
		}
	}

	private void generateSurface(World world, int posX, int posZ, Random random) {
		int posY = random.nextInt(world.getHeightValue(posX, posZ) + 32);
		for (int i = 0; i < 5; i++) {
			//System.out.println(posY);
			int x = posX + random.nextInt(16);
			int y = posY + random.nextInt(4) - random.nextInt(4);
			int z = posZ + random.nextInt(16);
			if (world.getBiomeGenForCoords(x, z) == BiomeGenBase.desert || world.getBiomeGenForCoords(x, z) == BiomeGenBase.desertHills)
				if (world.getBlock(x, y - 1, z) == Blocks.sand && (!world.provider.hasNoSky && y < 255) && world.isAirBlock(x, y, z)) {
					new BananaTreeGenerator(true).generate(world, random, x, y, z);
				}	
		}
	}
}
