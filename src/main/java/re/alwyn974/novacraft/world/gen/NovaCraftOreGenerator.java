/**
 * Copyright Alwyn974 2019-2020
 * 
 * @author Developed By <a href="https://github.com/alwyn974"> Alwyn974</a>
 */

package re.alwyn974.novacraft.world.gen;

import java.util.Random;

import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import re.alwyn974.novacraft.NovaCraft;

public class NovaCraftOreGenerator implements IWorldGenerator {

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator,IChunkProvider chunkProvider) {

		switch (world.provider.dimensionId) {
		case 0:
			this.generateSurface(world, chunkX * 16, chunkZ * 16, random);
			break;
		}
	}

	private void addOre(Block block, Block blockSpawn, Random random, World world, int posX, int posZ, int minY, int maxY, int FillonMax, int spawnChance) {
		for (int i = 0; i < spawnChance; i++) {
			int chunkSize = 16;
			int Xpos = posX + random.nextInt(chunkSize);
			int Ypos = minY + random.nextInt(maxY - minY);
			int Zpos = posZ + random.nextInt(chunkSize);

			new WorldGenMinable(block, FillonMax).generate(world, random, Xpos, Ypos, Zpos);
		}
	}

	private void generateSurface(World world, int x, int z, Random random) {
		addOre(NovaCraft.SpinelleOre, Blocks.stone, random, world, x, z, 2, 15, 4, 1);
		addOre(NovaCraft.MicroliteOre, Blocks.stone, random, world, x, z, 2, 15, 5, 2);
		addOre(NovaCraft.GraphiteOre, Blocks.stone, random, world, x, z, 2, 25, 7, 4);
		addOre(NovaCraft.RandomOre, Blocks.stone, random, world, x, z, 2, 64, 4, 8);
	}
}
